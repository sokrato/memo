# -*- coding: utf8 -*-
#
# 国家统计局 行政区域代码 文本:
#
# http://www.stats.gov.cn/tjsj/tjbz/xzqhdm/
#
from __future__ import print_function

from collections import OrderedDict


class Area(object):

    level = None

    def __init__(self, code, name, parent=None):
        '''use unicode name'''
        self.code = int(code)
        try:
            self.name = name.decode('utf8')
        except UnicodeEncodeError:
            self.name = name
        self.parent = parent

    def __unicode__(self):
        return u'%6d %1d %6d %s' % (self.code,
                self.level, self.parent.code, self.name)

    def __str__(self):
        return unicode(self).encode('utf8')


class ContainerArea(Area):

    sub_unit_kls = None

    def __init__(self, code, name, parent=None):
        super(ContainerArea, self).__init__(code, name, parent)
        self.subunits = OrderedDict()

    def add_sub_unit(self, code, name):
        code = int(code)
        unit = self.sub_unit_kls(code, name, self)
        self.subunits[code] = unit

    def get_sub_unit(self, code):
        code = int(code)
        return self.subunits[code]

    def __unicode__(self):
        ret = []
        if self.name == u'市辖区':
            ret.append(u'%6d %1d %6d %s' % (self.code,
                    self.level, self.parent.code, self.parent.name))
        elif self.name != u'县':
            ret.append(u'%6d %1d %6d %s' % (self.code,
                    self.level, self.parent.code, self.name))
        for code, unit in self.subunits.items():
            ret.append(unicode(unit))
        return u'\n'.join(ret)


class County(Area):

    level = 3


class City(ContainerArea):

    level = 2
    sub_unit_kls = County


class Province(ContainerArea):

    level = 1
    sub_unit_kls = City


class Nation(ContainerArea):

    sub_unit_kls = Province

    def __init__(self):
        super(Nation, self).__init__(0, u'中国', None)

    def load(self, istream):
        for line in istream:
            self.parse_entry(line)

    def parse_entry(self, line):
        line = line.strip()
        # ignore comments
        if not line or line.startswith('#'):
            return

        code, name = line.split()
        prov_part = int(code[:2])
        city_part = int(code[2:4])
        county_part = int(code[4:])

        if county_part == 0:
            if city_part == 0:  # province
                self.add_sub_unit(code, name)
            else:  # city
                prov_code = prov_part * 10000
                prov = self.get_sub_unit(prov_code)
                prov.add_sub_unit(code, name)
        else:
            prov_code = prov_part * 10000
            prov = self.get_sub_unit(prov_code)
            city_code = prov_code + city_part * 100
            city = prov.get_sub_unit(city_code)
            city.add_sub_unit(code, name)

    def __unicode__(self):
        return u'\n'.join([unicode(u) for u in self.subunits.values()])


if __name__ == '__main__':
    with open('nation.txt') as f:
        cn = Nation()
        cn.load(f)
        print(cn)
