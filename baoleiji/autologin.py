#!/usr/bin/env python
# encoding: utf8
from __future__ import print_function
import os, struct, fcntl, termios, signal, sys, getpass

try:
    prompt = raw_input
except NameError:
    prompt = input


def load_pexpect():
    try:
        import pexpect
    except ImportError:
        pexpect = None
    return pexpect


def load_otp():
    try:
        import pyotp
    except ImportError:
        pyotp = None
    return pyotp


def install_pip():
    print('installing pip...')
    cmd = '/bin/bash -c "curl https://bootstrap.pypa.io/get-pip.py -o /tmp/pip.py && python /tmp/pip.py"'
    ret = os.system(cmd)
    if ret:
        raise RuntimeError('install pip failed')


def load_requirements():
    pexpect, otp = load_pexpect(), load_otp()
    requirements = []
    if not pexpect:
        requirements.append('pexpect')
    if not otp:
        requirements.append('pyotp')
    if requirements:
        if os.system('which pip'):
            install_pip()
        req = ' '.join(requirements)
        print('installing %s' % req)
        cmd = 'pip install -U %s' % req
        if os.system(cmd):
            raise RuntimeError('failed to install %s' % req)
    return load_pexpect(), load_otp()


class App:
    def __init__(self, rcfile='~/.bljrc'):
        self.username = None
        self.otp_sec = None
        self.passwd = None
        # self.host = '172.16.8.106'
        self.host = '172.16.138.73'
        self.rcfile = os.path.expanduser(rcfile)
        self.child = None

    def setup_config(self):
        if os.path.exists(self.rcfile):
            self.load_config(self.rcfile)
        if not self.username:
            self.username = self.prompt(u'登录名（你的域账号）')
        if not self.passwd:
            self.passwd = self.prompt(u'域账号的密码 (不会回显)', secure=True)
        if not self.otp_sec:
            self.otp_sec = self.prompt(u'堡垒鸡 OTP Secret（如果不知道就找运维要）')
        if not self.host:
            self.host = self.prompt(u'堡垒鸡IP')
        self.save_config(self.rcfile)

    def prompt(self, key, secure=False):
        msg = u'%s: ' % key
        msg = msg.encode('utf8')
        p = getpass.getpass if secure else prompt
        while True:
            val = p(msg)
            val = val.strip()
            if val:
                break
        return val

    def save_config(self, rcfile):
        lines = [
            '# your login name',
            'username = %s' % self.username,
            '# your domain passwd',
            'passwd = %s' % self.passwd,
            '# your OTP secret, go to devops for it',
            'otp_sec = %s' % self.otp_sec,
            '# bao lei ji IP',
            'host = %s' % self.host,
        ]
        with open(rcfile, 'w+') as fout:
            fout.write('\n'.join(lines))

    def load_config(self, rcfile):
        with open(rcfile) as fin:
            for line in fin:
                line = line.strip()
                if not line or line.startswith('#'):
                    continue
                try:
                    key, val = line.split('=', 1)
                    key, val = key.strip(), val.strip()
                except ValueError:
                    raise RuntimeError('invalid config: %s' % line)
                if val.startswith('\'') and val.endswith('\''):
                    val = val[1:-1]
                setattr(self, key, val)

    def resize(self, *args):
        s = struct.pack("HHHH", 0, 0, 0, 0)
        a = struct.unpack('hhhh', fcntl.ioctl(sys.stdout.fileno(), termios.TIOCGWINSZ, s))
        self.child.setwinsize(a[0], a[1])

    def run(self):
        try:
            self.setup_config()
            pexpect, pyotp = load_requirements()
            cmd = 'ssh %s@%s' % (self.username, self.host)
            print(cmd)
            child = pexpect.spawn(cmd)

            self.child = child
            signal.signal(signal.SIGWINCH, self.resize)
            self.resize()

            child.expect('Verification code:')
            totp = pyotp.TOTP(self.otp_sec)
            child.sendline(totp.now())

            child.expect('Password:')
            child.sendline(self.passwd)
            child.interact(escape_character=None)
            child.wait()
        except KeyboardInterrupt:
            pass


App().run()
