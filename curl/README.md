
curlrc中必须用双引号将value括起来
如果output是gzip的，可通过gunzip -c -v -d "output.html"解压

common config options:

* -D --dump-header
* -A --user-agent
* -b --cookie
* -c --cookie-jar
* -H --header
* -i --include include headers
* -I --head fetch headers only
* -T --upload-file
* -v --verbose
* -K --config
* -o --output output contents to file
