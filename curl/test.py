#-*- encoding: utf-8 -*- 

from urllib2 import Request, urlopen, URLError
from urllib import urlencode
from cStringIO import StringIO
from gzip import GzipFile
import time, sys, os
import argparse

def getArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('url', help="the url to visit")
    parser.add_argument('-c', '--cookie', help='cookie string, or cookie @file', type=str)
    args = parser.parse_args()

    if not args.cookie:
        print "请输入cookie:"
        args.cookie = sys.stdin.readline().strip()
    return args

print args.url, args.cookie


