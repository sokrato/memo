#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

static void sig_handler(int signo)
{
    printf("signal #%d trapped. To quit, use `kill -9 %u`\n",
           signo, getpid());
}

int trap(int signo)
{
    if (signal(signo, sig_handler)==SIG_ERR)
        return -1;
    return 0;
}

int main(int argc, char* argv[])
{
    if (0 == trap(2)) {
        char buf[32];
        while (1) {
            fread(buf, 32, 1, stdin);
        }
    } else {
        fprintf(stderr, "cannot trap SIGINT\n");
        exit(1);
    }
    return 0;
}
