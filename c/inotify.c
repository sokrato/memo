#include <sys/inotify.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>


void usage(char* prog)
{
    printf("%s file/dir ...\n", prog);
}

#define pm(evt, m) do{ if(evt->mask&m) printf("%s, ", #m); } while(0)

void print_event(struct inotify_event * evt)
{
    if (evt->len > 0)
        printf("file: %s - \n", evt->name);
    pm(evt, IN_ACCESS);
    pm(evt, IN_ATTRIB);
    pm(evt, IN_CLOSE_WRITE);
    pm(evt, IN_CLOSE_NOWRITE);
    pm(evt, IN_CREATE);
    pm(evt, IN_DELETE);
    pm(evt, IN_DELETE_SELF);
    pm(evt, IN_MODIFY);
    pm(evt, IN_MOVE_SELF);
    pm(evt, IN_MOVED_FROM);
    pm(evt, IN_MOVED_TO);
    pm(evt, IN_OPEN);
    printf("\n---\n");
}

int watch(char *file)
{
    int inot = inotify_init();
    if (-1 == inot) {
        perror("inotify_init");
        return 1;
    }
    int wid = inotify_add_watch(inot, file, IN_ALL_EVENTS);
    if (-1 == wid) {
        perror("inotify_add_watch");
        return 2;
    }
    struct inotify_event * evt;
    size_t buflen = sizeof(struct inotify_event) + 32;
    if (NULL == (evt=calloc(1, buflen))) {
        perror("calloc");
        return 3;
    }
    while (1) {
        int ret = read(inot, evt, buflen);
        if (ret == 0) {
            perror("read"); // interupted
            return 4;
        }
        print_event(evt);
    }
    close(inot);
    return 0;
}

int main(int argc, char* argv[])
{
    if (argc < 2) {
        usage(argv[0]);
        return 0;
    }
    return watch(argv[1]);
}
