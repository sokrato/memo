#include <stdio.h>
#include <assert.h>
#include "xxdict.h"

typedef struct Person {
    char * name;
    int age;
} Person;

int main(int argc, char* argv[])
{
    char key[] = "``xiang";
    char * val = "hello there!";
    char * data;
    XXDict * people = xxdict_new();
    int i, j;
    for (i=0; i < 26; ++i) {
        key[0] += 1;
        key[1] = '`';
        for (j=0; j < 26; ++j) {
            key[1] += 1;
            xxdict_set(people, key, val);
            data = xxdict_get(people, key);
            printf("%s => %s\n", key, data);
        }
    }
    xxdict_del(people, key);
    data = xxdict_get(people, key);
    assert(data == NULL);
    xxdict_fin(people);
    return 0;
}

