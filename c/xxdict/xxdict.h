/**
 * References:
 * http://effbot.org/zone/python-hash.htm
 * https://hg.python.org/cpython/file/tip/Objects/dictobject.c
 * http://svn.python.org/projects/python/trunk/Objects/stringobject.c
 * http://www.laurentluce.com/posts/python-dictionary-implementation/
 */

#ifndef XXDICT_H
#define XXDICT_H

#define XXDICT_MINSIZE 16
#define XXDICT_PERTURB_SHIFT 5
#define XXDICT_LOAD_FACTOR 0.75

typedef struct XXDictEntry XXDictEntry;
typedef struct XXDict XXDict;

struct XXDictEntry {
    char * key;
    void * val;
    unsigned long hash;
};

struct XXDict {
    XXDictEntry * entries;
    unsigned long cap;
    unsigned long used;
    unsigned long mask;
};

XXDict * xxdict_new();
// set
int xxdict_set(XXDict *, char *, void *);
// get
void * xxdict_get(XXDict *, char *);
// delete
void xxdict_del(XXDict *, char *);
// destroy
void xxdict_fin(XXDict *);
unsigned long xxdict_hash_key(const char * key);
 
#endif

