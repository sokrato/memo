#include <assert.h>
#include <stdlib.h>
#include "xxdict.h"

#define entryIsEmpty(e) ((e)->key == NULL)
#define entryIsDummy(e) ((e)->key == (void*)-1)

int xxdict_resize(XXDict *dict, unsigned long newSize)
{
    XXDictEntry * entries = calloc(newSize, sizeof(XXDictEntry));
    if (entries == NULL)
        return 0;
    unsigned long oldCap = dict->cap;
    dict->cap = newSize;
    dict->mask = newSize - 1;

    unsigned long i;
    unsigned long j;
    unsigned long perturb;
    XXDictEntry * e;
    for (i=0; i < oldCap; ++i) {
        e = dict->entries + i;
        if (entryIsEmpty(e) || entryIsDummy(e)) {
            continue;
        }
        j = 0;
        perturb = e->hash;
        while (1) {
            j = (j << 2) + 1 + perturb;
            j &= dict->mask;
            if (entryIsEmpty(entries+j))
                break;
            perturb >>= XXDICT_PERTURB_SHIFT;
        }
        entries[j].key = e->key;
        entries[j].val = e->val;
        entries[j].hash = e->hash;
    }
    free(dict->entries);
    dict->entries = entries;
    return 1;
}

XXDict * xxdict_new()
{
    XXDict * dict = calloc(1, sizeof(XXDict));
    if (NULL == dict)
        return NULL;
    dict->entries = calloc(XXDICT_MINSIZE, sizeof(XXDictEntry));
    if (dict->entries == NULL) {
        free(dict);
        return NULL;
    }
    dict->cap = XXDICT_MINSIZE;
    dict->mask = XXDICT_MINSIZE - 1;
    return dict;
}

int xxdict_set(XXDict *dict, char * key, void * val)
{
    unsigned long hash = xxdict_hash_key(key);
    unsigned long perturb = hash;
    unsigned long index = 0;
    XXDictEntry * e;
    while (1) {
        index = (index << 2) + 1 + perturb;
        perturb >>= XXDICT_PERTURB_SHIFT;
        index &= dict->mask;
        e = dict->entries+index;
        if (entryIsEmpty(e) || entryIsDummy(e)) {
            e->key = key;
            e->hash = hash;
            break;
        }
        // found it
        if (e->hash == hash) {
            break;
        }
    }
    e->val = val;
    dict->used += 1;
    if (dict->used / (double)dict->cap > XXDICT_LOAD_FACTOR) {
        return xxdict_resize(dict, dict->cap * 2);
    }
    return 0;
}

void * xxdict_get(XXDict *dict, char * key)
{
    unsigned long i = 0;
    unsigned long hash = xxdict_hash_key(key);
    unsigned long perturb = hash;
    XXDictEntry * e;
    while (1) {
        i = (i << 2) + 1 + perturb;
        i &= dict->mask;
        e = dict->entries+i;
        if (entryIsEmpty(e))
            return NULL;
        if (entryIsDummy(e) || e->hash != hash) {
            perturb >>= XXDICT_PERTURB_SHIFT;
            continue;
        }
        return e->val;
    }
    assert(0); // should never reach here.
    return NULL;
}

void xxdict_del(XXDict * dict, char *key)
{
    unsigned long hash = xxdict_hash_key(key);
    unsigned long perturb = hash;
    unsigned long i = 0;
    XXDictEntry * e;
    while (1) {
        i = (i<<2) + 1 + perturb;
        i &= dict->mask;
        e = dict->entries + i;
        if (entryIsEmpty(e))
            return;
        if (e->hash == hash) {
            e->key = (void*) -1;
            break;
        }
        perturb >>= XXDICT_PERTURB_SHIFT;
    }
}

void xxdict_fin(XXDict * dict)
{
    free(dict->entries);
    free(dict);
}

unsigned long xxdict_hash_key(const char * key)
{
    if (NULL == key)
        return 0;
    unsigned long v = *key << 7;
    while (*key) {
        v = (10003 * v) ^ *key;
        ++key;
    }
    return v;
}

