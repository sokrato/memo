#include <iostream>
#include <string>

using namespace std;

namespace math {
int add(int a, int b) {
    cout << "add ints" << endl;
    return a + b;
}

string add(string a, string b) {
    cout << "add strings" << endl;
    return a + b;
}
};

int main() {
    string name;

    cout << "what is you name: " ;
    getline(cin, name);
    cout << "Hello, " << name << name.length() << endl;

    int a = 10, b = 20;
    cout << math::add(a, b) << endl;

    string sa = "hello", sb = "world";
    cout << math::add(sa, sb) << endl;

    // long long nums[0xFFFFFFFF]; // stack overflow
    return 0;
}
