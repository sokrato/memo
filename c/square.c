#include <stdio.h>

#define false 0
#define true 1
#define bool int

bool s2(int num)
{
    long NUM = (long)num;
    long root = num & 1 ? 1 : 2;;
    int step = 2;
    while (root * root < NUM) {
        // printf("%d\n", root);
        root += step;
    }
    return root * root == NUM;
}

bool isPerfectSquare(int num) {
    if (num < 0)
        return false;
    if (num < 2)
        return true;

    int lo = 2, hi = num /2;
    long pow, mid;
    while (lo <= hi) {
        mid = (lo + hi)/2;
        pow = mid * mid;
        printf("=> %d, %ld, %d, %ld\n", lo, mid, hi, pow);
        if (pow > num) {
            hi = mid - 1;
        } else if (pow == num) {
            return true;
        } else {
            lo = mid + 1;
        }
    }
    return false;
}

int main()
{
    bool (*fn)(int) = s2;
    int n = 2147483647;
    if (fn(n)) {
        printf("%d\n", n);
    }
    return 0;
}
