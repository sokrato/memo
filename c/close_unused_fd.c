#include <unistd.h>
#include <sys/resource.h>
#include <fcntl.h>
#include <stdio.h>

void show_nofile()
{
    struct rlimit rl;
    if (getrlimit(RLIMIT_NOFILE, &rl)) {
        perror("getrlimit");
    }
    printf("rlim_cur = %ld, rlim_max =%ld\n", rl.rlim_cur, rl.rlim_max);
}

int set_nofile(long cur, long max)
{
    struct rlimit rl;
    rl.rlim_cur = cur;
    rl.rlim_max = max;
    if (setrlimit(RLIMIT_NOFILE, &rl)) {
        perror("setrlimit");
        return -1;
    }
    return 0;
}

int close_unused_fd(int minfd, int maxfd)
{
    int fd;
    for (fd = minfd; fd<=maxfd; ++fd) {
        if (0 > fcntl(fd, F_GETFL))
            close(fd);
    }
    return 0;
}

#ifdef TEST
int main(int argc, char* argv[])
{
    return 0;
}
#endif
