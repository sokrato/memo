#include <stdio.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>

#ifdef DEBUG
#warning 'You are building this with DEBUG on!'
static void debug(char * argv[])
{
    char * * ptr = argv;
    while (NULL != *ptr) {
        printf("%s\n", *ptr);
        ++ptr;
    }
    printf("DONE.\n");
}
#else
    #define debug(xx)
#endif

int sudo(char * user, char * cmd, char * argv[])
{
    struct passwd *pw = getpwnam(user);
    if (NULL == pw) {
        printf("%s does not exist\n", user);
        return 1;
    }
    if (0 != setuid(pw->pw_uid)) {
        perror("setuid");
        return 2;
    }
    if (0 != execvp(cmd, argv)) {
        perror("execvp");
        return 3;
    }
    return 0;
}

int main(int argc, char* argv[])
{
    if (argc < 3) {
        fprintf(stderr, "%s user cmd arg1 arg2 ...\n", argv[0]);
        return 100;
    }
    debug(argv);
    return sudo(argv[1], argv[2], argv+2);
}

