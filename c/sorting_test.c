#include "sorting.h"
#include <stdio.h>


CompareResult compare(void* k, void* n) {
    int *ik=k, *in=n;
    // printf("CMP: %d, %d\n", *ik, *in);
    if (*ik > *in) return COMPARE_GREATER;
    else if (*ik < *in) return COMPARE_LESS;
    return COMPARE_EQUAL;
}

void test_binary_search() {
    int ns[5] = {1,2,3,4,5};
    void* a[3] = {ns, ns+1, ns+2};
    void* b[4] = {ns, ns+1, ns+2, ns+3};
    void* c[5] = {ns, ns+1, ns+2, ns+3, ns+4};

    int i=0;
    for ( ; i<7; ++i) {
        printf("%d at a[%d]", i, binary_search(a, 3, &compare, &i));
        printf(", %d at b[%d]", i, binary_search(b, 4, &compare, &i));
        printf(", %d at c[%d]\n", i, binary_search(c, 5, &compare, &i));
    }
}

int main() {
    test_binary_search();
    return 0;
}
