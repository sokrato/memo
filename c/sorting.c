#include "sorting.h"


CompareResult binary_search(void** arr, int len, CompareFunc fn, void* key) {
    int l=0, r=len-1, m;
    CompareResult cmp;
    while (l<=r) {
        m = (l+r)/2;
        // printf("bin: %d, %d, %d\n", l, r, m);
        cmp = fn(key, arr[m]);
        if (cmp == COMPARE_EQUAL)
            return m;
        else if (cmp == COMPARE_GREATER)
            l = m + 1;
        else
            r = m - 1;
    }
    return NOT_FOUND;
}
