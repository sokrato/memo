#include <stdio.h>

void fn1()
{
    printf("fn1\n");
}

void fn2()
{
    printf("fn2\n");
}

typedef void (*Func)();

int main()
{
// #define fn fn2
    Func fn = fn1;
    fn();
    return 0;
}
