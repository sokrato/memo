
typedef enum Color {
    COLOR_WHITE,
    COLOR_BLACK
} Color;

int main() {
    Color c = COLOR_BLACK;
    int a = 1, b= 3;
    const int * p1 = &a;
    int * const p2 = &a;
    *p1 = 2; // assignment of read-only location ‘*p1’
    p2 = &b; // assignment of read-only variable ‘p2’
    return c;
}
