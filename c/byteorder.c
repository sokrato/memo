#include <stdio.h>

#define SLEN sizeof(short)

int main()
{
    union {
        short s;
        char cs[SLEN];
    } v;
    v.s = 0x0102;
    if (SLEN == 2) {
        if (v.cs[0]==1 && v.cs[1]==2) {
            printf("big endian\n");
        } else if (v.cs[0]==2 && v.cs[1]==1) {
            printf("little endian\n");
        } else {
            printf("endian unknown\n");
        }
    } else {
        printf("sizeof short = %zu\n", SLEN);
    }
    return 0;
}
