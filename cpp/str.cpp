#include <iostream>

char* fn() {
    return (char*)"hello";
}

char* fn2() {
    char *s = (char*)"hi";
    return s;
}

int main() {
    char *s = fn2();
    std::cout << s << std::endl;
    s[0] = 'o';
    std::cout << s << std::endl; // segmentation fault!
    return 0;
}
