#include <iostream>
#include <vector>
#include <initializer_list>

int main() {
    for (int i : {1, 2, 3, 4})
        std::cout << i;
    std::cout << std::endl;

    std::vector<int> ns = {1, 3, 5};
    for (auto i : ns) {
        std::cout << i;
    }
    return 0;
}
