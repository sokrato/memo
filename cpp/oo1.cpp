#include <iostream>
#include <string>


class Person {
    private:
        unsigned int id;
        int age;
        std::string name;
        static unsigned int counter;

        static int nextID() { return ++counter/*++Person::counter*/; }

    public:
        Person(int age, std::string name) throw(std::string) {
            if (age < 1) {
                throw std::string("negative age?");
            }
            this->id = Person::nextID(); //++Person::counter;
            this->age = age;
            this->name = name;
        }

        ~Person() {
            std::cout << "destructor called on " << name << std::endl;
        }

        void sayHi() {
            // std::cout << "Hello from " << name << ", age:" << age << ", ID:" << id << std::endl;
            std::cout << "Hello! ";
            std::cout << *this;
        }

        friend Person& operator+(Person& p, int n);

        friend std::ostream& operator<<(std::ostream& out, const Person& p);
};

Person& operator+(Person& p, int n) {
    p.age += n;
    return p;
}

std::ostream& operator<<(std::ostream& out, const Person& p) {
    out << "Person name: " << p.name
        << ", age: " << p.age
        << ", ID: " << p.id
        << std::endl;
    return out;
}

unsigned int Person::counter = 0;

int main() {
    try {
        Person me(7, "xuxiang");
        me.sayHi();
        throw std::string("bala");
    } catch(std::string err) {
        std::cerr << "Error: " << err << std::endl;
    }
    Person me(27, "xuxiang");
    me = me + 3 + 9;
    me.sayHi();

    Person* m2 = new Person(27, "xu xiang");
    m2->sayHi();
    // delete me;

    return 0;
}
