#include <stdio.h>
#include <stdint.h>

int main() {
    printf("%lu, %lu\n", sizeof(int8_t), sizeof(int32_t));
    return 0;
}
