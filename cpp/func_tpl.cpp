#include <cassert>
#include <cstring>

template <class T>
void swap(T *a, T *b) {
    if (a == b) return;
    char *buf = new char[sizeof(T)];
    memcpy(buf, a, sizeof(T));
    memcpy(a, b, sizeof(T));
    memcpy(b, buf, sizeof(T));
}

template <class T>
void swap2(T &a, T &b) {
    T tmp = a;
    a = b;
    b = tmp;
}

int main() {
    int a = 10, b = 100;
    swap(&a, &b);
    assert(b==10 && a==100);

    swap2(a, b);
    assert(a==10 && b==100);
    return 0;
}
