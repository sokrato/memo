function accumulate(start)
    return coroutine.create(function(start)
        local sum = start or 0
        local i=0
        while true do
            sum = i + sum
            print('co', i, sum)
            i = coroutine.yield(sum)
        end
        return sum
    end)
end
