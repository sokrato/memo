-- metatable example

local mt = {}
Set = {}

function Set.new(t)
    local ret = {}
    setmetatable(ret, mt)
    for k, v in pairs(t) do
        ret[v] = true
    end
    return ret
end

function Set.union(t1, t2)
    local ret = {}
    setmetatable(ret, mt)
    for _, t in ipairs{t1, t2} do
        for k, _ in pairs(t) do
            ret[k] = true
        end
    end
    return ret
end

function Set.tostring(s)
    local t = {}
    for i, _ in pairs(s) do
        t[#t + 1] = i
    end
    return table.concat(t, ', ')
end

function Set.print(s)
    print(Set.tostring(s))
end

mt.__add = Set.union
mt.__tostring = Set.tostring

s1 = Set.new{1,2,3,1}
s2 = Set.new{2,3,3,4}

s3 = s1 + s2

Set.print(s3)
