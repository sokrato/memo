local N = 8 -- board size

local function isplaceok(brd, row, col)
    for i = 1, row-1 do
        if (brd[i] == col) or
           (brd[i] - i == col - row) or
           (brd[i] + i == col + row) then
           return false
        end
    end
    return true
end


local function printsolution(a)
    for i = 1, N do
        for j = 1, N do
            io.write(a[i] == j and "X" or "-", " ")
        end
        io.write("\n")
    end
    io.write("\n")
end


local function addqueen(a, n)
    if n > N then
        printsolution(a)
    else
        for c = 1, N do
            if isplaceok(a, n, c) then
                a[n] = c
                addqueen(a, n+1)
            end
        end
    end
end

addqueen({}, 1)
