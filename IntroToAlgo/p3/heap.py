# encoding: utf-8

class Heap(object):
    def __init__(self, data=None):
        self._data = data or []
        if self._data:
            self.heapify(self._data)

    @classmethod
    def heapify(cls, arr, start=1):
        for i in range(start, len(arr)):
            index = i
            while index > 0:
                parent_index = (index-1)//2
                if arr[parent_index] > arr[index]:
                    arr[parent_index], arr[index] = arr[index], arr[parent_index]
                    index = parent_index
                else:
                    break

    def add(self, val):
        self._data.append(val)
        self.heapify(self._data, len(self._data) - 1)

    def peak(self):
        if not self._data:
            raise IndexError
        return self._data[0]

    def pop(self):
        val = self.peak()
        if len(self._data) > 1:
            self._data[0], self._data[-1] = self._data[-1], self._data[0]
        self._data.pop(-1)
        self.heapify(self._data)
        return val

    def print(self):
        print(self._data)

    @classmethod
    def test(cls):
        heap = cls([3,1,2,5,5,9,8,7])
        heap.print()
        print(heap.peak())
        heap.add(0)
        heap.print()
        while heap._data:
            print(heap.pop(), heap._data)


if __name__ == '__main__':
    Heap.test()
