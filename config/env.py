#-*- coding: utf -*-
from __future__ import print_function
'''
Script to setup a working environment.

Install these packages:
    build-essential (gcc, etc.)
    mysql-server-5.6
    sqlite3

Save these config files:
    vimrc
    screenrc
    gitconfig
    bashrc


https://pypi.python.org/packages/source/v/virtualenv/virtualenv-1.11.4.tar.gz#md5=9accc2d3f0ec1da479ce2c3d1fdff06e
https://raw.githubusercontent.com/dlutxx/notes/master/config/gitconfig
'''

import os
from multiprocessing import Process


def _save_rc(name):
    url = 'https://raw.githubusercontent.com/dlutxx/memo/master/config/%s' % name
    os.system('curl %s >> .%s' % (url, name))

def save_rc():
    ps = []
    for rc in 'vimrc,screenrc,gitconfig,bashrc'.split(','):
        p = Process(target=_save_rc, args=(rc,))
        p.start()
        ps.append(p)
    [p.join() for p in ps]

def main():
    save_rc()


if __name__ == '__main__':
    main()
