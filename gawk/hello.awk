#whose work time > 0
#$3 == 0 {print $1, $2*$3}
#$3 > 0 {printf("%02d %-8s $%6.2f\n", NR, $1, $2*$3)}

NF == 3 {pay += $2*$3; rows += 1}

END {
    if (rows>0)
        avg = pay/rows;
    else
        avg = 0;
    printf("%d avg: %6.2f\n", rows, avg);
}
