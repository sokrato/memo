Shell related notes
======

1. Use sshpass to bypass ssh password input. 
2. sometimes you cannot use scp or rsync or rcp to transfer file between hosts,
   you can still transfer with nc and awk:
   1. first listen to receive a file with nc: nc -l 0.0.0.0 8080 > recv.file
   2. send file with awk: gawk '{printf("%s", $0) > "/inet/tcp/0/localhost/8080"}' send.file
3. randomly permute lines of a file: awk 'BEGIN{srand()} {rs[int(rand()*100), NR]=NR " " $0;}END{asorti(rs, inds);for(i in inds){print rs[inds[i]];}}' input
4. *nix queues only one instance of SIGCHLD signal, so if you try to `wait` for all child processes, use this pattern:
   > while (waitpid((pid_t)-1, NULL, WNOHANG)>0) {}

   to exhaust all exited child procs. PHP's signal handler do not interupt pcntl_wait call, be ware!
