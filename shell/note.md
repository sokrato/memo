[Classic Shell Scripting Notes]

auto start GNU screen: add this to .profile - `[ -z "$STY" ] && screen -Rd "work"`

echo - display a line of text
-n - no trailing \r\n
-e escape special sequences

stty - change and print terminal line settings
stty [-]echo turn on/off echo

simple execution tracing
bash -x script, or set -x/+x to turn on/off
