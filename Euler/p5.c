#include <stdio.h>

int divisible(long n)
{
    int i;
    for (i=2; i<21; ++i)
        if (n%i!=0)
            return 0;
    return 1;
}

int main()
{
    long n;
    for (n=2520; n<465675841; n+=10)
        if (divisible(n))
            break;
    printf("%ld\n", n);
    return 0;
}
