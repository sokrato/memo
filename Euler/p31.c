#include <stdio.h>

int coins[] = {1, 2, 5, 10, 20, 50, 100};

int find(int amount, int use) {
    if (use == 0) return 1;
    int n = 0, i=0;
    for (i=use; i>=0; --i) {
        if (amount - coins[i] == 0) ++n;
        if (amount - coins[i] > 0) n += find(amount - coins[i], i);
    }
    return n;
}

main() {
    printf("%d\n", find(200, 6));
}
