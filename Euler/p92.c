#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MAX 10*1000*1000

char chain(int n, char* memo)
{
    if (n == 1 || n == 89) return n;
    if ( n>= MAX || n==0) {
        printf("Error: n=%d\n", n);
        exit(1);
    }
    if (memo[n]==0) {
        int c = n, r, sum=0;
        while (c) {
            r = c%10;
            c = (c-r)/10;
            sum += r*r;
        }
        memo[n] = chain(sum, memo);
    }
    return memo[n];
}

int main(int argc, char* argv[])
{
    int roof = strtol(argv[1], NULL, 10);
    char *memo = calloc(MAX, sizeof(char));
    int counter = 0, i;
    assert(memo != NULL);
    for (i=1; i<roof; ++i) {
        if (chain(i, memo)==89)
            ++counter;
    }
    printf("%d\n", counter);
    free(memo);
    return 0;
}
