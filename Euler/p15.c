/**
 * Combinatorial solution:
 * 0 - right 1 - down
 * 0011 0101 0110
 * 1001 1010 1100
 * C(n,n/2)
 * n = 20
 **/
#include <stdio.h>

#define LEN 21
int main()
{
    long routes[LEN][LEN];
    int r, c;
    for (r=0; r<LEN; ++r)
        routes[r][0] = 1;
    for (c=0; c<LEN; ++c)
        routes[0][c] = 1;
    for (r=1; r<LEN; ++r)
        for (c=1; c<LEN; ++c)
            routes[r][c] = routes[r-1][c] + routes[r][c-1];
    printf("%ld\n", routes[LEN-1][LEN-1]);
    return 0;
}
