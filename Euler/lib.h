#ifndef _LIB_H
#define _LIB_H

#define max(a, b) ((a)>(b) ? (a) : (b))

int is_prime(long n);

void qsort(int*, int, int);

void print_arr(int*, int);

#endif
