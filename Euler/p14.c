#include <stdio.h>

int collatz(long n)
{
    int c = 0;
    while (n>1) {
        if (n%2==0) n /= 2;
        else n = 3*n+1;
        ++c;
    }
    return c;
}

int main()
{
    int max=0, i, n=0, t;
    for (i=2; i<=1e6; ++i) {
        t = collatz(i);
        if (max<t) {
            max = t;
            n = i;
        }
    }
    printf("%d, %d\n", n, max);
    return 0;
}
