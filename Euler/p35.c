#include "lib.h"
#include <stdio.h>

int is_circular_prime(int n)
{
    if (!is_prime(n)) return 0;
    int order=-1, power=1, n2=n;
    while (n2>0) {
        n2 /= 10;
        ++order;
        power *= 10;
    }
    power /= 10;
    for (n2=0; n2<order; ++n2) {
        n = n/power + (n%power)*10;
        if (!is_prime(n)) return 0;
    }
    return 1;
}

int main()
{
    int i, count=1;
    for (i=3; i<1000000; i+=2) {
        if (is_circular_prime(i))
            ++count;
    }
    printf("%d\n", count);
    return 0;
}
