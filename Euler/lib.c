#include "lib.h"
#include <stdio.h>

int is_prime(long n)
{
    if (n==2 || n==3 || n==5)
        return 1;
    if (n<2 || n%2==0 || n%3==0 || n%5==0)
        return 0;
    long f;
    for (f=7; f*f<=n; f+=2)
        if (n%f==0)
            return 0;
    return 1;
}

static int qsort_partition(int *arr, int left, int right)
{
    int i = left - 1, j, tmp;
    int x = arr[right];
    for (j=left; j<right; ++j) {
        if (arr[j] <= x) {
            ++i;
            if (i != j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
    }
    if (i < right-1) {
        tmp = arr[i+1];
        arr[i+1] = arr[right];
        arr[right] = tmp;
    }
    return i+1;
}

void qsort(int* arr, int start, int end)
{
    if (start < end) {
        int i = qsort_partition(arr, start, end);
        qsort(arr, start, i-1);
        qsort(arr, i+1, end);
    }
}

void print_arr(int *arr, int len)
{
    while (len-->0) {
        printf("%d, ", *(arr++));
    }
    printf("\n");
}
