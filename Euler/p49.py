from __future__ import print_function

from itertools import combinations, permutations

def isprime(n):
    if n<2: return False
    if n in (2, 3, 5, 7): return True
    if any((n%i==0 for i in (2, 3, 5, 7))): return False
    i = 3
    while i*i <= n:
        if n%i==0: return False
        i += 2
    return True

if __name__ == '__main__':
    pool = tuple(range(10))
    for comb in combinations(pool, 4):
        solution = {}
        for p in permutations(comb):
            n = sum((p[i]*10**(3-i) for i in range(4)))
            if n>999 and isprime(n):
                if solution.get(p[3], None):
                    solution[p[3]].append(n)
                else:
                    solution[p[3]] = [n]
        for i, s in solution.iteritems():
            print(i, s)
