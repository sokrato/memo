package main

import (
	"fmt"
)

var (
	cache  = map[int]int{}
	maxKey = 0
	maxVal = 0
)

func pent(n int) int {
	return n * (3*n - 1) / 2
}

func is_pent(n int) bool {
	if n <= maxVal {
		return cache[n] > 0
	}
	for ; maxVal <= n; maxKey++ {
		maxVal = pent(maxKey)
		cache[maxVal] = maxKey
	}
	return is_pent(n)
}

func main() {
OUTER:
	for i := 1; ; i++ {
		pi := pent(i)
		pi2 := pent(i - 1)

		for j := i - 1; j > 0; j-- {
			pj := pent(j)
			if (pi - pj) > pi2 {
				break
			}
			if is_pent(pi-pj) && is_pent(pi+pj) {
				fmt.Println(i, j, pi-pj)
				break OUTER
			}
		}
	}
}
