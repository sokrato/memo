from __future__ import print_function

import sys


class Card(object):

    ranks = {}
    r = 1
    for c in '23456789TJQKA':
        ranks[c] = r
        r += 1

    def __init__(self, face, suit):
        self.face = face
        self.rank = Card.ranks[face]
        self.suit = suit

    def __str__(self):
        return '<%s, %s>' % (self.face, self.suit)

    __repr__ = __str__

    def __lt__(self, that):
        return self.rank < that.rank

    def __le__(self, that):
        return self.rank <= that.rank

    def __eq__(self, that):
        return self.rank == that.rank and self.suit == that.suit

    @classmethod
    def from_str(kls, s):
        return kls(s[0], s[1])


class Hand(object):

    HIGH_CARDS = 1
    ONE_PAIR = 2
    TWO_PAIRS = 3
    THREE = 4
    STRAIGHT = 5
    FLUSH = 6
    FULL_HOUSE = 7
    FOUR = 8
    STRAIGHT_FLUSH = 9

    def __init__(self, cards):
        assert len(cards) == 5, 'invalid cards'
        self.cards = tuple(sorted(cards))
        self.level = self.calc_level()

    def calc_level(self):
        c1, c2, c3, c4, c5 = self.cards
        if c5.suit == c4.suit and c4.suit==c3.suit and c3.suit==c2.suit and c2.suit==c1.suit:
            if c5.rank==c4.rank+1 and c4.rank==c3.rank+1 and c3.rank==c2.rank+1 and c2.rank==c1.rank+1:
                return (self.STRAIGHT_FLUSH, c5.rank)
            return (self.FLUSH, c5.rank, c4.rank, c3.rank, c2.rank, c1.rank)

        nums = {}
        for c in self.cards:
            if c.rank in nums:
                nums[c.rank] += 1
            else:
                nums[c.rank] = 1

        if len(nums) == 5:
            if c5.rank==c4.rank+1 and c4.rank==c3.rank+1 and c3.rank==c2.rank+1 and c2.rank==c1.rank+1:
                return (self.STRAIGHT, c5.rank)
            return (self.HIGH_CARDS, c5.rank, c4.rank, c3.rank, c2.rank, c1.rank)
        if len(nums) == 4:
            rank = [k for k, v in nums.iteritems() if v ==2][0]
            nums.pop(rank)
            cnt = 0
            for r in nums.keys():
                cnt += 13**r
            return (self.ONE_PAIR, rank, cnt)
        if len(nums) == 3:  # 2 pairs or 3 of a kind
            three = [k for k, v in nums.iteritems() if v==3]
            if three:
                rank = three[0]
                nums.pop(rank)
                cnt = sum((13**i for i in nums.keys()))
                return (self.THREE, rank, cnt)
            # two pairs
            ls = [k for k, v in nums.iteritems() if v==2]
            cnt = 0
            for i in ls:
                cnt += 13**i
                nums.pop(i)
            return (self.TWO_PAIRS, cnt, nums.keys()[0])
        if len(nums) == 2:  # four of a kind, or full house
            if any(map(lambda i: i==4, nums.values())):  # four of a kind
                rank = [k for k, v in nums.iteritems() if v == 4][0]
                nums.pop(rank)
                return (self.FOUR, rank, nums.keys()[0])
            # full house
            rank = [k for k, v in nums.iteritems() if v == 3][0]
            nums.pop(rank)
            return (self.FULL_HOUSE, rank, nums.keys()[0])
        raise Exception('impossible')

    @classmethod
    def from_str(kls, ss):
        cards = [Card.from_str(s) for s in ss]
        return kls(cards)

    def __lt__(self, other):
        return self.level < other.level


if __name__ == '__main__':
    cnt = 0
    for line in open('p054_poker.txt'):
        parts = line.strip().split()
        left = Hand.from_str(parts[:5])
        rite = Hand.from_str(parts[5:])
        if left > rite:
            cnt += 1
    print(cnt)
