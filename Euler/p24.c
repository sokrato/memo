#include "lib.h"
#include <stdio.h>

static int count = 0;

void perm(int* memo, int level)
{
    if (level == 10) {
        if(++count==1000000)
            print_arr(memo, 10);
        return;
    }
    int i, j;
    for (i=0; i<10; ++i) {
        for (j=0; j<level; ++j)
            if (memo[j] == i)
                goto next;
        memo[level] = i;
        perm(memo, level+1);
next:
        continue;
    }
}

int main()
{
    static int memo[10];
    perm(memo, 0);
    return 0;
}
