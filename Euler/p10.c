#include "lib.h"
#include <stdio.h>

int main()
{
    long sum=17, i;
    for (i=11; i<2e6; i+=2)
        if (is_prime(i))
            sum += i;
    printf("%ld\n", sum);
    return 0;
}
