from __future__ import print_function

def isprime(n):
    if n<2: return False
    if n in (2, 3, 5, 7): return True
    if any((n%i==0 for i in (2, 3, 5, 7))): return False
    i = 3
    while i*i <= n:
        if n%i==0: return False
        i += 2
    return True

def primes(roof=1e6):
    yield 2
    p = 3
    while p <= roof:
        if isprime(p):
            yield p
        p += 2


def main(below=1e6):
    ls = []
    for i in primes(below):
        ls.append(i)
    # print(len(ls)) 78498
    maxPrime = ls[-1]
    length = len(ls)
    longest = 0
    biggest = 0
    for i in range(length):
        j = i
        sum = 0
        n = 0
        while j<length and sum<=maxPrime:
            n += 1
            sum += ls[j]
            if sum in ls and n>longest:
                longest = n
                biggest = sum
            j += 1
    print(longest, biggest)

if __name__ == '__main__':
    main()
