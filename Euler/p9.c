/*
 * ProjectEuler: 9
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 * a2 + b2 = c2
 * For example, 32 + 42 = 9 + 16 = 25 = 52.
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
*/

#include <stdio.h>

int main()
{
    int a, b;
    for (a=1; a<1000; ++a)
        for (b=a+1; b<(1000-a); ++b)
            if (1e6 == (2000*(a+b) - 2*a*b))
                printf("%d, %d\n", a, b);
    return 0;
}
