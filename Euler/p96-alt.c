#include <stdio.h>
#include <string.h>

#define LEN 9

typedef enum Bool {NO=0, YES} Bool;

typedef struct Board {
    char grid[LEN][LEN+2];
} Board;

typedef struct Info {
    char stuffed;
    char emptyRow;
    char emptyCol;
} Info;

void display(Board *brd)
{
    int r, c;
    for (r=0; r<LEN; ++r) {
        if (r==3 || r==6)
            printf("- - - - - - - - - - -\n");
        for (c=0; c<LEN; ++c) {
            if (c==3 || c==6)
                printf("| ");
            printf("%d ", brd->grid[r][c]);
        }
        printf("\n");
    }
    printf("\n");
}

void display_infos(Info *inf)
{
    int i;
    for (i=0; i<LEN; ++i)
        printf("(%d, %d, %d) ", inf[i].stuffed, inf[i].emptyRow, inf[i].emptyCol);
    printf("\n");
}

int find_max_occupied_non_full_idx(Info *inf, int size);

Bool solve(Board *brd, Info *rows, Info *cols, Info *blks)
{
    int r, c, b;
    memset(rows, 0, LEN*sizeof(Info));
    memset(cols, 0, LEN*sizeof(Info));
    memset(blks, 0, LEN*sizeof(Info));
    for (r=0; r<LEN; ++r)
        for (c=0; c<LEN; ++c) {
            b = r/3*3 + c/3;
            if (brd->grid[r][c]) {
                ++rows[r].stuffed;
                ++cols[c].stuffed;
                ++blks[b].stuffed;
            } else {
                rows[r].emptyRow = cols[c].emptyRow = blks[b].emptyRow = r;
                rows[r].emptyCol = cols[c].emptyCol = blks[b].emptyCol = c;
            }
        }
    int maxr, maxc, maxb, nr, nc;
    maxr = find_max_occupied_non_full_idx(rows, LEN);
    if (maxr<0) return YES;
    maxc = find_max_occupied_non_full_idx(cols, LEN);
    maxb = find_max_occupied_non_full_idx(blks, LEN);
    Info *inf;
    inf = rows[maxr].stuffed>cols[maxc].stuffed ? rows+maxr : cols+maxc;
    inf = inf[0].stuffed > blks[maxb].stuffed ? inf : blks+maxb;
    nr = inf[0].emptyRow;
    nc = inf[0].emptyCol;
    maxr = nr/3*3;
    maxc = nc/3*3;
    for (b=1; b<LEN+1; ++b) {
        // check row
        for (c=0; c<LEN; ++c)
            if (brd->grid[nr][c] == b)
                goto next;
        // check col
        for (r=0; r<LEN; ++r)
            if (brd->grid[r][nc] == b)
                goto next;
        // check block
        for (r=0; r<3; ++r)
            for (c=0; c<3; ++c)
                if (brd->grid[r+maxr][c+maxc]==b)
                    goto next;
        brd->grid[nr][nc] = b;
        if (solve(brd, rows, cols, blks))
            return YES;
next:
        continue;
    }
    brd->grid[nr][nc] = 0;
    return NO;
}

int find_max_occupied_non_full_idx(Info *inf, int size)
{
    int i, max=0;
    for (i=1; i<size; ++i)
        if ((inf[max].stuffed<inf[i].stuffed && inf[i].stuffed<LEN)
                || inf[max].stuffed==LEN) {
            max = i;
        }
    if (inf[max].stuffed==LEN) return -1;
    return max;
}

int main(int argc, char *argv[])
{
    FILE *file;
    Board brd;
    Info rows[LEN], cols[LEN], blks[LEN];
    if (argc>1) {
        file = fopen(argv[1], "rb");
        if (file == NULL) {
            perror("open error");
            return 1;
        }
    } else
        file = stdin;
    int i, t, sum;
    while (fgets(brd.grid[0], LEN+2, file)) {
        for (i=0; i<LEN; ++i) {
            fgets(brd.grid[i], LEN+2, file);
            for (t=0; t<LEN; ++t) // normalization
                brd.grid[i][t] -= '0';
        }
        if (solve(&brd, rows, cols, blks)==NO) {
            printf("failed\n");
            return 1;
        }
        sum += brd.grid[0][0]*100 + brd.grid[0][1]*10 + brd.grid[0][2];
    }
    printf("sum is %d\n", sum);
    return 0;
}
