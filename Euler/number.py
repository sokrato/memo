# -*- coding: utf8 -*-
from __future__ import absolute_import, print_function

import math

# small primes
primes = (2, 3, 5, 7, 11, 13, 17)
_prime_set = set(primes)

def is_prime(n):
    if n < 2:
        return False
    if n in _prime_set:
        return True
    for i in primes:
        if n % i == 0:
            return False
    i = primes[-1]
    while i * i <= n:
        if n % i == 0:
            return False
        i += 2
    return True


# 18   [(3, 2), (2, 1)]
def factors(num):
    if num < 4:
        return [(num, 1)]

    memo = []
    num, twos = strip_factor(num, 2)
    while num > 1:
        sqrt = int(math.sqrt(num))
        if sqrt % 2 == 0:
            sqrt -= 1

        found = False
        while sqrt > 2:
            if num % sqrt == 0 and is_prime(sqrt):
                num, times = strip_factor(num, sqrt)
                memo.append((sqrt, times))
                found = True
                break
            sqrt -= 2

        if not found:
            memo.append((num, 1))
            break

    if twos:
        memo.append((2, twos))
    return memo


def strip_factor(num, factor):
    # return striped num and time of factor
    if num < 2:
        return num, 0

    times = 0
    while True:
        div, mod = divmod(num, factor)
        if mod:
            break
        times += 1
        num = div
    return num, times


def factors_pretty(num):
    fs = factors(num)
    ret = []
    for f, t in reversed(fs):
        ret.extend([f] * t)
    return ret


def factorial(n):
    ret = 1
    while n > 1:
        ret *= n
        n -= 1
    return ret


def test():
    print(factors(7))
    print(factors(25))
    print(factors_pretty(10004350063))

    f = factorial(7)
    print(factors_pretty(f))


if __name__ == '__main__':
    test()
