#include "common.h"

#define MAX 10000
static int Cache[MAX*10];

int amicableSum(int n) {
    if (Cache[n] > 0)
        return Cache[n];
    int sum = 1;
    for (auto i=2; i<=n/2; ++i) {
        if (n % i == 0)
            sum += i;
    }
    Cache[n] = sum;
    return Cache[n];
}

int main() {
    int total = 0;
    for (auto i=2; i <= MAX; ++i) {
        int s1 = amicableSum(i);
        if (s1 <= i)
            continue;
        if (amicableSum(s1) == i)
            total += i + (s1 <= MAX ? s1 : 0);
    }
    std::cout << "Total = " << total << std::endl;
}
