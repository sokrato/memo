#include "common.h"


bool isPrime(long n)
{
    if (n==2 || n==3 || n==5)
        return YES;
    if (n<2 || n%2==0 || n%3==0 || n%5==0)
        return NO;
    long f;
    for (f=7; f*f<=n; f+=2)
        if (n%f==0)
            return NO;
    return YES;
}

void printArray(int *arr, int size) {
    while (size-- > 0) {
        std::cout << *arr++ << ", ";
    }
    std::cout << std::endl;
}

int power(int base, int expo) {
    if (0 == expo) return 1;
    if (1 == expo) return base;
    if (2 == expo) return base * base;
    if (0 == expo%2) {
        int res = power(base, expo/2);
        return res * res;
    } else {
        int res = power(base, expo/2);
        return res * res * base;
    }
}

void fillPrimes(int *arr, int size) {
    if (size < 1) return;
    arr[0] = 2;
    if (1 == size) return;
    int i, n = 3;
    for (i=1; i < size; ++i) {
        while (YES != isPrime(n)) {
            n += 2;
        }
        arr[i] = n;
        n += 2;
    }
}
