#include "common.h"
#include <set>

int countDigits(int n) {
    int c;
    for (c=0; n > 0; ++c, n/=10);
    return c;
}

int padNum(int num, int ndigits) {
    int nd = countDigits(num);
    if (nd >= ndigits)
        return num;
    int delta = ndigits - nd;
    return num * power(10, delta);
}

int recipLength(int n) {
    std::set<int> rmd;
    int divend = 1;
    int ndigs = countDigits(n) + 1;
    // std::cout << "\n*** " << n << std::endl;
    for (;;) {
        divend = padNum(divend, ndigs);
        int remainder = divend % n;
        if (0 == remainder)
            break;
        if (rmd.count(remainder) > 0) { // loop found
            break;
        }
        divend = remainder;
        rmd.insert(remainder);
        // std::cout << divend << ", ";
    }
    return rmd.size();
}

int main() {
    int maxi{0}, maxLen{0};
    for (auto i=3; i<1000; i+=2) {
        int len = recipLength(i);
        if (len > maxLen) {
            maxi = i;
            maxLen = len;
        }
    }
    std::cout << maxi << std::endl;
}
