#include "common.h"

// I just tried several time to get this number
static const int NPrimes = 12499;
static int primes[NPrimes];

int countDistinctFactors(int num) {
    int o=num;
    int n=0, i=0, div;
    for( ; num >= primes[i]; ++i) {
        if (i >= NPrimes) {
            std::cout << o << ", " << i << std::endl;
            return 0/0; // intentionly to abort
        }
        div = num/primes[i];
        if (div * primes[i] != num)
            continue;
        ++n; // found one prime factor
        num = div;
        do {
            div = num/primes[i];
            if (div*primes[i] == num)
                num = div;
            else
                break;
        } while (num > 1);
    }
    return n;
}

int main() {
    fillPrimes(primes, NPrimes);
    int n, nconsec;
    for (n=646; ; ++n) {
        if (4 == countDistinctFactors(n)) {
            if (++nconsec == 4)
                break;
        } else {
            nconsec = 0;
        }
    }
    std::cout << n << std::endl;
    return 0;
}
