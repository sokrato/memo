#-*- coding: utf8 -*-

from __future__ import print_function, division

class Board(object):
    def __init__(self, grid):
        self.grid = grid

    def normalize(self):
        for r in range(0, 9):
            for c in range(0, 9):
                self.grid[r][c] = int(self.grid[r][c])

    def display(self):
        for r in range(0, 9):
            if r==3 or r==6:
                print('- '*11)
            for c in range(0, 9):
                if c==3 or c==6:
                    print('|', end=' ')
                print(self.grid[r][c], end=' ')
            print('')
        print('')

    def solve(self):
        rowInfos = [Info(i, 0) for i in range(0, 9)]
        colInfos = [Info(i, 0) for i in range(0, 9)]
        blkInfos = [Info(i, 0) for i in range(0, 9)]
        for r in range(0, 9):
            for c in range(0, 9):
                b = int(r/3)*3 + int(c/3)
                v = self.grid[r][c]
                if v:
                    rowInfos[r].incr()
                    colInfos[c].incr()
                    blkInfos[b].incr()
                else:
                    rowInfos[r].set_point(r, c)
                    colInfos[c].set_point(r, c)
                    blkInfos[b].set_point(r, c)
        xr = self.find_max_occupied_but_not_full(rowInfos)
        if xr is None:
            return True
        xc = self.find_max_occupied_but_not_full(colInfos)
        xb = self.find_max_occupied_but_not_full(blkInfos)
        mini = self.find_max_occupied_but_not_full((xr, xc, xb))
        for i in range(1, 10):
            if self.check_num_at(i, mini.r, mini.c):
                self.grid[mini.r][mini.c] = i
                if self.solve():
                    return True
        self.grid[mini.r][mini.c] = 0
        return False

    def check_num_at(self, n, r, c):
        for i in range(0, 9):
            if self.grid[r][i] == n:
                return False
            if self.grid[i][c] == n:
                return False
        br = int(r/3)*3
        bc = int(c/3)*3
        for r in range(br, br+3):
            for c in range(bc, bc+3):
                if self.grid[r][c] == n:
                    return False
        return True

    def find_max_occupied_but_not_full(self, ls):
        maxi = ls[0]
        for i in ls[1:]:
            if maxi.num < i.num < 9 or maxi.num==9:
                maxi = i
        return maxi if maxi.num<9 else None

class Info(object):
    def __init__(self, idx, num):
        self.idx, self.num = idx, num
        self.r = self.c = -1

    def set_point(self, r, c):
        self.r, self.c = r, c

    def incr(self):
        self.num += 1
        return self.num

    def decr(self):
        self.num -= 1
        return self.num

    def __lt__(self, i):
        return self.num < i.num

    def __str__(self):
        return '(%d, %d, %d, %d)' % (self.idx, self.num, self.r, self.c)

    def __repr__(self):
        return self.__str__()

if __name__ == '__main__':
    import sys
    with open('files/sudoku.txt' if len(sys.argv)==1 else sys.argv[1]) as file:
        sum = 0
        item = 0
        while True:
            if not file.readline():
                break
            item += 1
            gd = []
            n = 0
            while n<9:
                gd.append(list(file.readline().strip()))
                n += 1
            brd = Board(gd)
            brd.normalize()
            assert brd.solve()
            brd.display()
            sum += brd.grid[0][0]*100 + brd.grid[0][1]*10 + brd.grid[0][2]
            print(item, sum)
        print(sum)
