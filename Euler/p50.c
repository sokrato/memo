#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct Primer {
    int length;
    int size;
    int slots[];
} Primer;

int isPrime(int n)
{
    if (n==2 || n==3 || n==5 || n==7) return 1;
    if (n%2==0 || n%3==0 || n%5==0 || n%7==0) return 0;
    int p;
    for (p=3; p*p<=n; p+=2)
        if (n%p==0) return 0;
    return 1;
}

Primer* sieve(int max)
{
    Primer *p = calloc(sizeof(Primer) + 1024*sizeof(int), 1), *p2;
    assert(p != NULL);
    int n;
    p->size = 1024;
    p->slots[0] = 2;
    p->length = 1;
    for (n=3; n<=max; n+=2) {
        if (isPrime(n)) {
            if (p->length == p->size) {
                p2 = calloc(sizeof(Primer) + (p->size + 1024)*sizeof(int), 1);
                assert(p2!=NULL);
                memcpy(p2, p, sizeof(Primer)+(p->size*sizeof(int)));
                p2->size += 1024;
                free(p);
                p = p2;
            }
            p->slots[p->length] = n;
            ++p->length;
        }
    }
    return p;
}

int binsearch(int* arr, int length, int val)
{
    int left=0, right=length-1, mid;
    while (left<=right) {
        mid = (left + right)/2;
        if (arr[mid] == val) return mid;
        if (arr[mid] > val) right = mid - 1;
        if (arr[mid] < val) left = mid + 1;
    }
    return -1;
}

/**
 * This C program is WAY faster than the Python equivalent
 */
int main(int argc, char* argv[])
{
    Primer* primes = sieve(1000*1000);
    // printf("%d, %d\n", primes->size, primes->length);
    int maxPrime = 0, longest = 0;
    int sum = 0, length = 0;
    int i, j;
    for (i=0; i<primes->length; ++i) {
        sum = primes->slots[i];
        length = 1;
        for (j=i+1; j<primes->length && sum<primes->slots[primes->length-1]; ++j) {
            sum += primes->slots[j];
            length += 1;
            if (binsearch(primes->slots, primes->length, sum)>-1 && length>longest) {
                maxPrime = sum;
                longest = length;
            }
        }
    }
    printf("%d, %d\n", longest, maxPrime);
    return 0;
}
