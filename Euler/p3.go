// ProjectEuler: 3
// The prime factors of 13195 are 5, 7, 13 and 29.
// What is the largest prime factor of the number 600851475143 ?

package main

import (
	"log"
	"math"
)

var (
	primes = []int64{2, 3, 5, 7, 11}
)

func bin_search(n int64, slots []int64) int {
	left, rite := 0, len(slots)-1
	for left <= rite {
		mid := (left + rite) / 2
		if n == slots[mid] {
			return mid
		} else if n > slots[mid] {
			left = mid + 1
		} else {
			rite = mid - 1
		}
	}
	return -1
}

func is_prime(n int64) bool {
	/* if n < 2 {
		return false
	} */
	if bin_search(n, primes) > -1 {
		return true
	}
	for _, p := range primes {
		if n%p == 0 {
			return false
		}
	}
	for i := primes[len(primes)-1] + 2; i*i <= n; i += 2 {
		if n%i == 0 {
			return false
		}
	}
	return true
}

// max prime factor
func mpf(n int64) int64 {
	if is_prime(n) || n <= 2 {
		return n
	}
	root := int64(math.Sqrt(float64(n)))
	if root*root == n {
		return mpf(root)
	}

	var step int64 = 2
	if root%2 == 0 {
		step = 1
	}

	for f := root; n > 2; f -= step {
		if n%f == 0 {
			f2 := n / f
			f, f2 := mpf(f), mpf(f2)
			if f > f2 {
				return f
			}
			return f2
		}
	}
	return 0
}

func main() {
	log.Println(mpf(600))
	log.Println(mpf(600851475143))
}
