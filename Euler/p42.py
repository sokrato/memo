from __future__ import print_function

import math

def isTriangleNumber(n):
    x = math.sqrt(2*n+0.25) - 0.5
    return x==int(x)

def isTriangleWord(w):
    s = 0
    for i in w.upper():
        s += ord(i) - 64
    return isTriangleNumber(s)

def main():
    n = 0
    with open('words.txt') as f:
        line = f.read().strip().split(',')
        for w in line:
            if isTriangleWord(w.replace('"', '')):
                n += 1
                print(w)
    return n

if __name__ == '__main__':
    print(main()) # 162
