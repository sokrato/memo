#include <stdio.h>

#define INPUT "sudoku.txt"
#define LINE 16
#define SIZE 9

/**
 * return 1 if found (and modify row, col), 0 otherwise (leaving row & col as was)
 */
int find_empty_pos_since(char board[][11], int *row, int *col)
{
    int x=*col, y=*row;
    for (; y<SIZE; ++y)
    {
        if (y>*row)
            x = 0;
        for (; x<SIZE; ++x)
            if (board[y][x]=='0')
                goto found;
    }
found:
    ;
    if (x<SIZE && y<SIZE) {
        *row = y;
        *col = x;
        return 1;
    }
    return 0;
}

int _solve(char board[][11], int row, int col)
{
    if (!find_empty_pos_since(board, &row, &col))
        return 0;
    int t, i, j, nextRow, nextCol;
    int x, y;
    y = row/3*3;
    x = col/3*3;
    if (col < SIZE)
    {
        nextRow = row;
        nextCol = col + 1;
    } else {
        nextRow = row + 1;
        nextCol = 0;
    }
    for (t='1'; t<='9'; ++t)
    {
        // check row
        for (i=0; i<9; ++i)
            if (board[row][i] == t)
                goto next;
        // check col
        for (i=0; i<9; ++i)
            if (board[i][col] == t)
                goto next;
        // check square
        for (i=y; i<y+3; ++i)
            for (j=x; j<x+3; ++j)
                if (board[i][j] == t)
                    goto next;
        board[row][col] = t;
        if (_solve(board, nextRow, nextCol)==0) {
            return 0;
        }
next:
        continue;
    }
    board[row][col] = '0';
    return 1;
}

int solve(char board[9][11])
{
    if (_solve(board, 0, 0))
    {
        printf("cant be solved\n");
        return -1;
    }
    return 100*(board[0][0]-'0') + 10*(board[0][1]-'0') + board[0][2]-'0';
}

int main(int argc, char *argv[])
{
    FILE *file;
    char board[9][11];
    long sum = 0;
    int i;
    if ((file = fopen(INPUT, "rb"))==NULL)
    {
        perror("cant open input file");
        return 1;
    }
    while (fgets(board[0], 11, file))
    {
        for (i=0; i<9; ++i)
            fgets(board[i], 11, file);
        sum += solve(board);
    }
    printf("sum is %ld\n", sum);
    fclose(file);
    return 0;
}
