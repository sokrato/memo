from __future__ import print_function

from itertools import permutations


def check(pm):
    primes = (2, 3, 5, 7, 11, 13, 17)
    for i in range(len(primes)):
        if (pm[i+1]*100 + pm[i+2]*10 + pm[i+3]) % primes[i] != 0:
            return False
    return True


if __name__ == '__main__':
    s = 0
    for i in range(1, 10):
        nine = list(range(10))
        nine.remove(i)
        for pm in permutations(nine):
            pm = (i,) + pm
            if check(pm):
                s += sum((pm[i]*10**(9-i) for i in range(10)))
    print(s)
