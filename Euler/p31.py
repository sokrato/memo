#-*- coding: utf8 -*-
# !ProjectEuler Coin Sums
from __future__ import print_function, division

curs = (200, 100, 50, 20, 10, 5, 2, 1)

def sums(goal, cpos, solutions, solution=None):
    solution = solution or tuple()
    total = sum([i*j for i,j in solution])
    left = goal - total
    # print(goal, curs, solution, solutions)
    if left == 0:
        slt = tuple([(i, j) for i, j in solution if i>0])
        solutions.add(slt)
    elif left < 0:
        return
    if cpos >= len(curs): return
    tryCur = curs[cpos]
    for i in range(0, int(left/tryCur)+1):
        slt = solution + ((i, tryCur),)
        sums(goal, cpos+1, solutions, slt)


def find2(goal, use):
    if use == len(curs):
        return 1
    n = 0
    i = use
    while i < len(curs):
        if goal - curs[i] == 0:
            n += 1
        elif goal - curs[i] > 0:
            n += find2(goal-curs[i], i)
        i += 1
    return n


def main():
    print(find2(200, 0))
    print(sums(200, 0, solutions=solutions))


if __name__ == '__main__':
    main()
