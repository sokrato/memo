package main

import (
    "fmt"
    "time"
    "os"
)

var coins = [...]int{200, 100, 50, 20, 10, 5, 2, 1}

func find(goal, index int) int {
    if index == 8 {
        return 1
    }
    n := 0
    for i := index; i < 8; i ++ {
        if goal - coins[i] == 0 {
            n++
        } else if  goal - coins[i] > 0 {
            n += find(goal-coins[i], i)
        }
    }
    return n
}

func main() {
    fmt.Printf("%d\n", find(200, 0))
    to := time.After(time.Second * 3)
    ok := make(chan bool, 1)
    go func() {
        bs := make([]byte, 16)
        n, _ := os.Stdin.Read(bs)
        if n > 0 {
            os.Stdout.Write(bs[:n])
        }
        ok<- true
    }()
    select {
    case <-to:
        fmt.Println("timeout")
    case <- ok:
        fmt.Println("read ok")
    }
}
