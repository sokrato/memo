#include <stdio.h>
#include "lib.h"

long max_prime_factor(long n)
{
    long i, p=0;
    if (n%2==0) p = 2;
    for (i=3; i*i<n; i+=2)
        if (n%i==0 && is_prime(i))
            p = i;
    return p;
}

// gcc -Wall lib.o -I. p3.c
int main(int argc, char* argv[])
{
    printf("%ld\n", max_prime_factor(600851475143));
    return 0;
}
