#include <stdio.h>

long p2(int max)
{
    int a=1, b=1, c;
    long sum=0;
    while (b<=max) {
        c = a;
        a = a+b;
        b = c;
        if (a%2 == 0) 
            sum += a;
    }
    return sum;
}

int main()
{
    printf("%ld", p2(4e6));
    return 0;
}
