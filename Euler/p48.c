#include <stdio.h>

#define MASK 10000000000

long long last_ten(n)
{
    long long r = 1;
    int i;
    for (i=0; i<n; ++i) {
        r *= n;
        if (r > MASK)
            r %= MASK;
    }
    return r;
}
int main()
{
    long long sum=0;
    int i;
    for (i=1; i<1001; ++i) {
        sum += last_ten(i);
        if (sum > MASK)
            sum %= MASK;
    }
    printf("%lld\n", sum);
    return 0;
}
