# rest api framework
import copy


RTYPE_RAW = 0
RTYPE_BIN = 1
RTYPE_TEXT = 2
RTYPE_JSON = 4


class Options(object):
    def __init__(self, meta):
        self.meta = meta
        self.apis = {}

    def add_api(self, name, request):
        self.apis[name] = request


class Request(object):
    def __init__(self, method, path=None, args=None, params=None, data=None, json=None, headers=None, rtype=None):
        self.method = method
        self.path = path
        self.rtype = rtype
        self.args = args
        self.params = params
        self.data = data
        self.json = json
        self.headers = headers
        self.client = None

    def copy(self):
        return Request(self.method,
                self.path,
                copy.copy(self.args),
                copy.copy(self.params),
                copy.copy(self.data),
                copy.copy(self.json),
                copy.copy(self.headers),
                self.rtype
                )

    def __call__(self, *args):
        print('\n--API Called!--')
        print(args)


class Get(Request):
    def __init__(self, path=None, **kwargs):
        super(Get, self).__init__('get', path, **kwargs)


class Post(Request):
    def __init__(self, path=None, **kwargs):
        super(Get, self).__init__('post', path, **kwargs)


class RestMeta(type):

    @staticmethod
    def path_from_name(name):
        return name.replace('_', '/')

    def __new__(cls, name, bases, attrs):
        print('\nRestMeta:', name)
        print('dict =', cls.__dict__)
        print(cls, bases, attrs)
        meta = Options(getattr(cls, 'Meta', None))
        for name, req in attrs.items():
            if isinstance(req, Request):
                if req.path is None:
                    req.path = cls.path_from_name(name)
                meta.add_api(name, req)
        return super(RestMeta, cls).__new__(cls, name, bases, attrs)


class RestBase(metaclass=RestMeta):
    NAME = 'RestBase'

    def __new__(cls, *args):
        print('RestBase __new__:', cls, args)
        inst = super(RestBase, cls).__new__(cls)
        return inst


class RPCardClient(RestBase):
    # URL_BASE = 'http://localhost:8080/api/'
    URL_BASE = 'https://rpcardapi.u51.com/rpcard-gateway/api/v1/'
    request_executor = None

    def __init__(self, uid, auth):
        super(RPCardClient, self).__init__()
        print('__init__ in rpc')
        self.uid = uid
        self.auth = auth

    home = Get()
    # homepage = Get('homepage', headers, params, data, files, json)
    homepage = Get('homepage')  # , headers, params, data, files)
    # 合成宝石
    # jewel_synthetic = Post('jewel/synthetic', ('type', 'level'))
    # 签到
    # sign = Post('sign', ("latitude", "longitude", "poiAddress", "poiId", "poiName"))


service = RPCardClient(1, 2)
service.home()
service.homepage('homepage')
print(service.home.path)
print(RPCardClient.path_from_name)

