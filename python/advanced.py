#-*- coding: utf8 -*-
'''
Examples of advanced Python features:
  - metaclass
  - descriptor
  - generator/forloop
'''
from __future__ import print_function

import sys

if sys.version_info > (3, ):  # Python 3
    exec('''
def exec_in(code, glob, loc=None):
    if isinstance(code, str):
        code = compile(code, '<string>', 'exec', dont_inherit=True)
    exec(code, glob, loc)
''')
    exec_in('''
def with_meta(cls):
    class Meta(metaclass=cls):
        pass
    return Meta
    ''', globals())
else:
    exec('''
def exec_in(code, glob, loc=None):
    if isinstance(code, str):
        code = compile(code, '', 'exec', dont_inherit=True)
    exec code in glob, loc
''')
    exec_in('''
def with_meta(cls):
    class Meta(object):
        __metaclass__ = cls
        pass
    return Meta
''', globals())


class AnimalMeta(type):

    species = 0

    def __new__(cls, name, bases, attrs):
        if not name == 'Meta':
            cls.species += 1
            print(
                'First, metaclass.__new__ received (metaclass, name, bases, attrs)')
            print(cls, name, bases, attrs)
        return super(AnimalMeta, cls).__new__(cls, name, bases, attrs)

    def __init__(self, name, bases, attrs):
        if not name == 'Meta':
            print(
                'Second, metaclass.__init__ received (self, name, bases, attrs)')
            print(self, name, bases, attrs)

    def __call__(self, *args, **kwargs):
        print("AnimalMeta.__call__")
        return super(AnimalMeta, self).__call__(*args, **kwargs)


class Cat(with_meta(AnimalMeta)):

    name = 'cat'

    def __init__(self):
        print('Meow')


kit = Cat()
