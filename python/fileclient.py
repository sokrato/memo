#-8- encoding: utf-8 -*-
import sys
import os
import socket
import argparse
import logging


class FileSender(object):
    logger = None

    def __init__(self, host, port, logger=None):
        self.host = host
        self.port = port
        self.logger = logger or self.getDefaultLogger()

    @classmethod
    def getDefaultLogger(cls):
        if not cls.logger:
            logger = logging.getLogger(cls.__name__)
            logger.setLevel(logging.DEBUG)
            sh = logging.StreamHandler()
            sh.setLevel(logging.DEBUG)
            formatter = logging.Formatter(
                '%(levelname)s - %(asctime)s - %(name)s - %(message)s')
            sh.setFormatter(formatter)
            logger.addHandler(sh)
            cls.logger = logger
        return cls.logger

    def send(self, *files):
        for f in files:
            self.sendfile(f)

    def sendfile(self, file):
        try:
            stats = os.stat(file)
            size = stats.st_size
        except IOError as err:
            self.logger.error(err.msg)
            return
        f = open(file, 'rb')
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.host, self.port))
        s.send('{}\t{}'.format(file, size))

        ready = s.recv(256)
        if 'ready' == ready:
            self.logger.info('start sending {}, {} bytes'.format(file, size))
        else:
            self.logger.error('server not responding correctly')
            f.close(), s.close()
            return

        nbytes = 0
        while True:
            buf = f.read(1024)
            s.send(buf)
            nbytes += len(buf)
            if nbytes >= size:
                break
        self.logger.info('{} bytes sent'.format(nbytes))
        f.close(), s.close()


if '__main__' == __name__:
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=str, help="file to send")
    parser.add_argument(
        '-H',
        '--host',
        dest='host',
        type=str,
        action='store',
        default='127.0.0.1',
        help="file server name")
    parser.add_argument(
        '-P',
        '--port',
        dest='port',
        type=int,
        action='store',
        default=8888,
        help="file server port")
    args = parser.parse_args()
    client = FileSender(args.host, args.port)
    client.send(args.file)
