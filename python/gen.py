''' snippet about usage of yield keyword '''
from __future__ import print_function
from contextlib import contextmanager
import functools


@contextmanager
def valve(key=None):
    print("valve opened with", key)
    try:
        yield key
    except Exception as e:
        print("error:", e)
    finally:
        print("valve closed. key =", key)


with valve(123) as key:
    print("key is", key)
    raise RuntimeError("wtf!")


# efficient fibonacci series generator
def fibo():
    a, b = 0, 1
    while True:
        yield b
        a, b = b, a + b


def echo(value=None):
    print("Execution starts when 'next()' is called for the first time.")
    try:
        while True:
            try:
                value = (yield value)
            except Exception as e:
                value = e
    finally:
        print("Don't forget to clean up when 'close()' is called.")


# emulates tornado's coroutine
def coroutine(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        g = fn(*args, **kwargs)
        try:
            val = None
            while True:
                val = g.send(val)
        except StopIteration:
            pass

    return wrapper


def test_echo():
    gen = echo(1)
    print(gen.next())  # 1
    print(gen.next())  # None
    print(gen.send(10))  # 10
    print(gen.throw(ValueError('wtf')))  # ValueError('wtf')
    gen.close()
