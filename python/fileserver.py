#-*- encoding: utf-8 -*-
import sys
import argparse
import socket
import logging
from threading import Thread
from os import path


class ProtocolException(Exception):
    pass


class BadCommand(ProtocolException):
    pass


class FileWorker(Thread):
    '''
    1.client: filename\tfilesize
    2.server: ready
    3.client: filecontents
    4.server: received N bytes
    '''

    def __init__(self, sock, addr, dir, logger, *args, **kwargs):
        self.sock, self.addr = sock, addr
        self.logger = logger
        self.buf_size = 1024
        self.dir = dir
        super(FileWorker, self).__init__(*args, **kwargs)

    def run(self):
        self.sock.settimeout(5)

        try:
            cmd = self.sock.recv(1024)
            line = cmd.strip().split('\t')
            if 2 != len(line):
                raise BadCommand(cmd)
            filename, filesize = line[0], line[1]
            filename = path.basename(filename.strip())
        except BadCommand as ex:
            self.logger.warn('bad command: {}'.format(ex.message))
            self.sock.send(b'bad command')
            self.sock.close()
            return
        if not filename:
            self.sock.send(b'no command received')
            self.sock.close()
            return
        filesize = int(filesize)
        self.logger.info('starting to recv {} bytes'.format(filesize))
        self.sock.send('ready')

        nbytes = 0
        file = open(path.join(self.dir, filename), 'wb')
        try:
            while True:
                bs = self.sock.recv(self.buf_size)
                file.write(bs)
                nbytes += len(bs)
                if nbytes >= filesize:
                    break
        except socket.timeout:
            file.close()
            self.sock.close()
            return
        self.sock.send('received {} bytes'.format(nbytes))
        self.logger.info('written {} bytes to {}'.format(nbytes, filename))
        file.close()
        self.sock.close()


class FileServer(object):
    logger = None

    def __init__(self,
                 host='localhost',
                 port=8080,
                 dir=None,
                 backlog=10,
                 logger=None):
        self.host, self.port, self.backlog = host, port, backlog
        self.serv_sock = None
        self.logger = logger or FileServer.getDefaultLogger()
        self.dir = dir or '.'
        self.dir = path.realpath(self.dir)
        if not path.isdir(self.dir):
            raise ValueError('invalid path {}'.format(self.dir))

    @classmethod
    def getDefaultLogger(cls):
        if not cls.logger:
            logger = logging.getLogger(cls.__name__)
            logger.setLevel(logging.DEBUG)
            sh = logging.StreamHandler()
            sh.setLevel(logging.DEBUG)
            formatter = logging.Formatter(
                '%(levelname)s - %(asctime)s - %(name)s - %(message)s')
            sh.setFormatter(formatter)
            logger.addHandler(sh)
            cls.logger = logger
        return cls.logger

    def start(self):
        self.serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serv_sock.bind((self.host, self.port))
        self.serv_sock.listen(self.backlog)

        while True:
            sock, addr = self.serv_sock.accept()
            self.process(sock, addr)

    def process(self, sock, addr):
        self.logger.info('connection from {}'.format(addr))
        FileWorker(sock, addr, self.dir, self.logger).start()


if '__main__' == __name__:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-H',
        '--host',
        dest='host',
        type=str,
        action='store',
        default='0.0.0.0',
        help='The host to bind to')
    parser.add_argument(
        '-P',
        '--port',
        dest='port',
        type=int,
        action='store',
        default=8888,
        help='Which port to listen on')
    parser.add_argument(
        '--dir',
        dest='dir',
        type=str,
        action='store',
        default=None,
        help='where to store uploaded files')
    args = parser.parse_args()
    serv = FileServer(args.host, args.port, args.dir)
    serv.start()
