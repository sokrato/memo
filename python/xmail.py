# encoding: utf8
''' Python3 only
Auto mail sending.
'''

import datetime, logging
import imaplib, smtplib
import time, base64
from email.parser import Parser
from email.message import EmailMessage


logging.basicConfig(level=logging.INFO)

MAIL_TPL = '''<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<p>Hi，all<br/> {date}业务反欺诈数据如下：</p>
<p class=note style="color: gray;">
*名词注释：<br/>
请求量=通过量+验证量+拒绝量：用户发起的订单总量<br/>
验证量（用户）：命中规则并且需要验证的订单（用户）数量<br/>
通过量（用户）：经过规则无需验证的订单（用户）数量<br/>
拒绝量（用户）：经过规则被直接拒绝，未能进入验证流程的订单（用户）数量<br/>
拦截量=拒绝量+验证量=拒绝量+验证成功量+验证失败量：遭遇规则后需要验证或者直接拒绝的订单数量<br/>
拦截率=拦截量/请求量<br/>
验证成功量（用户）：命中规则进入验证环节且验证通过的订单（用户）数量<br/>
验证失败量（用户）：命中规则进入验证环节且验证未通过（包括放弃验证和验证失败）的订单（用户）数量<br/>
验证成功（失败）率=验证成功（失败）量/验证量<br/></p>
<div class=section><h3>1、用户登录业务数据：  <h3><img src="cid:0"><br/><img src="cid:1"></div>
<div class=section><h3>2、ALL IN还款业务数据：<h3><img src="cid:2"><br/><img src="cid:3"></div>
<div class=section><h3>3、人品宝转账业务数据：<h3><img src="cid:4"><br/><img src="cid:5"></div>
<div class=section><h3>4、人品宝还款业务数据：<h3><img src="cid:6"><br/><img src="cid:7"></div>
</body>
<html>'''

class App:

    imap_host = 'mail.u51.com'
    imap_port = 993
    smtp_host = imap_host
    smtp_port = 587
    log = logging.getLogger(__name__)

    def __init__(self, name, passwd):
        self.name = name
        self.passwd = passwd
        self.from_email = '%s@u51.com' % name
        self.cc_email = (
            # 'test@u51.com',
        )
        self.to_email = (
            # 'test@u51.com',
        )
        self.search_subject = '反欺诈数据日报'
        date = datetime.date.today().strftime('%d-%b-%Y').lstrip('0')
        self.search_cond = [
            '(HEADER FROM "table@u51.com")',
            '(SINCE "%s")' % date,
        ]

    def decode(self, msg):
        'input: =?utf-8?B?5b6Q57+U?= <test@u51.com>'
        self.log.info('decoding: %s', msg)
        prefix = '=?utf-8?b?'
        if msg[:len(prefix)].lower() == prefix:
            pos = msg.index('?=')
            text = msg[len(prefix): pos]
            text = base64.b64decode(text).decode('utf8')
            return '%s%s' % (text, msg[pos+2:])
        return msg

    def fetch_mail_matching_subject(self, conn, ids, subject):
        parser = Parser()
        for id in ids:
            msg = conn.fetch(id, "(RFC822)")
            mail = parser.parsestr(msg[1][0][1].decode('ascii'))
            self.log.debug('Got mail: %s', mail.items())
            subject = self.decode(mail.get('Subject'))
            self.log.info('subject: %s', subject)
            if subject == self.search_subject:
                return mail
        return None

    def imap_conn(self):
        conn = imaplib.IMAP4_SSL(self.imap_host, self.imap_port)
        conn.login(self.name, self.passwd)
        conn.select('INBOX')
        return conn

    def find_mail(self, retry=5, sleep=180):
        conn = self.imap_conn()
        for i in range(retry):
            try:
                code, [msg_ids] = conn.search(None, *self.search_cond)
                if code != 'OK':
                    raise RuntimeError('failed to find mails: %s' % code)
                if not msg_ids:
                    raise RuntimeError('empty search result')

                self.log.info('msg_ids: %s', msg_ids)
                msg_ids = reversed(msg_ids.split())
                mail = self.fetch_mail_matching_subject(conn, msg_ids, self.search_subject)
                if mail:
                    return mail
            except Exception as e:
                self.log.error('error finding mail: %s', e)
            time.sleep(sleep)
        raise RuntimeError('No matching mail found.')

    def compose_mail(self, images):
        msg = EmailMessage()
        date = datetime.date.today() - datetime.timedelta(days=1)
        msg['Subject'] = '%s %d-%d-%d' % (self.search_subject, date.year, date.month, date.day)

        msg['From'] = self.from_email
        msg['To'] = self.to_email
        msg['CC'] = self.cc_email
        msg['BCC'] = 'test <test@u51.com>'
        msg.set_content('you should be viewing an html email')
        html = MAIL_TPL.format(date='%d月%d日' % (date.month, date.day))
        msg.add_alternative(html, subtype='html')

        body = msg.get_payload()[1]
        for i in range(8):
            img = images[i].get_payload(decode=1)
            self.log.info('adding img#%d, size=%d', i, len(img))
            body.add_related(img, 'image', 'png', cid='<%d>' % i)
        return msg

    def process_mail(self, mail):
        if not mail.is_multipart():
            raise ValueError('mail is not multipart')
        self.log.info('processing %s', mail.items())
        payloads = mail.get_payload()
        if len(payloads) != 9:
            raise ValueError('mail should contain 9 payloads, got %d' % len(payloads))

        return self.compose_mail(payloads[1:])

    def send_mail(self, mail):
        self.log.info('Sending: %s', mail.items())
        with smtplib.SMTP(self.smtp_host, self.smtp_port) as smtp:
            smtp.ehlo()
            smtp.starttls()
            smtp.ehlo()
            self.log.info('smtp login: %s', smtp.login(self.name, self.passwd))
            self.log.warning('send: %s', smtp.send_message(mail))

    def main(self):
        mail = self.find_mail()
        mail = self.process_mail(mail)
        self.send_mail(mail)
        self.log.info('Done.')


if '__main__' == __name__:
    App('test user', "test pwd").main()

