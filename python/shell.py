#!env python
# encoding: utf8
import re
import os
import sys
import signal
import shlex


class Shell:
    VAR_PAT = re.compile(r'\$([a-zA-Z_]\w*|[\?\$\!])')

    def __init__(self, env=None, stdin=None, stdout=None, stderr=None):
        self.env = dict(env or os.environ)
        self.stdin = stdin or sys.stdin
        self.stdout = stdout or sys.stdout
        self.stderr = stderr or sys.stderr
        self.env['$'] = str(os.getpid())

    def write(self, msg, dest='stdout', flush=True):
        dest = self.stdout if dest == 'stdout' else self.stderr
        dest.write(msg)
        if flush:
            dest.flush()

    def substitue_var(self, text):
        ret, pos = [], 0
        for m in self.VAR_PAT.finditer(text):
            start, end = m.span()
            ret.append(text[pos:start])
            key = m.group(0)[1:]
            ret.append(self.env.get(key, ''))
            pos = end
        ret.append(text[pos:])
        return ''.join(ret)

    def cmd_export(self, args):
        for item in args:
            parts = item.split('=')
            if len(parts) == 2:
                self.env[parts[0]] = parts[1]

    def cmd_echo(self, args):
        for i in range(len(args)):
            it = self.substitue_var(args[i])
            self.write(it)
            if i < len(args) - 1:
                self.write(' ')
        self.write('\n')

    def cmd_exit(self, args):
        raise SystemExit()

    def close(self, *fds):
        for fd in fds:
            try:
                os.close(fd)
            except OSError:
                pass

    def execute(self, cmd, args):
        fds = os.pipe()
        try:
            pid = os.fork()
        except OSError as e:
            self.write(str(e), dest='stderr')
            self.close(*fds)

        if pid > 0:  # parent
            os.dup2(self.stdin.fileno(), fds[1])
            self.close(fds[0])
            st = 0
            while True:
                try:
                    _, st = os.waitpid(pid, 0)
                    break
                except KeyboardInterrupt:
                    os.kill(pid, signal.SIGINT)
                except OSError:
                    st = 1
            self.env['?'] = str(st)
            self.close(fds[1])
        else:  # child
            self.close(fds[1])
            try:
                if self.stdin.fileno() != fds[0]:
                    os.dup2(fds[0], self.stdin.fileno())
            except Exception as e:
                self.write(str(e), dest='stderr')
                raise SystemExit()
            args.insert(0, cmd)
            try:
                os.execvpe(cmd, args, self.env)
            except OSError as e:
                self.write(str(e), dest='stderr')

    def read_commands(self):
        prompt = self.env.get('PS1', '>> ')
        self.write(prompt)
        line = sys.stdin.readline()
        if not line:
            return None
        ret = shlex.split(line)
        for i in range(len(ret)):
            ret[i] = str(ret[i])
        return ret

    def evaluate(self, commands):
        if commands is None:
            self.write('\n')
            raise SystemExit
            return
        if not commands:
            return

        cmd, *args = commands
        meth = getattr(self, 'cmd_' + cmd, None)
        if meth:  # builtin commands
            meth(args)
            return

        self.execute(cmd, args)

    def repl(self):
        while True:
            commands = self.read_commands()
            self.evaluate(commands)


if __name__ == '__main__':
    env = dict(os.environ)
    env['PS1'] = 'myshell>> '
    sh = Shell(env)
    sh.repl()
