import logging

logging.basicConfig()


# https://docs.python.org/3.5/howto/descriptor.html
class Counter(object):
    def __init__(self, cnt=0):
        self.cnt = cnt

    def __get__(self, obj, obj_type=None):
        logging.warn('getting cnt: obj=%s, type=%s', obj, obj_type)
        self.cnt += 1
        return self.cnt

    def __set__(self, obj, value):
        raise NotImplementedError('not allowed')

    def __delete__(self, obj):
        pass


class Apple(object):
    count = Counter(1)


class Bone(object):
    count = Counter(1)

    def __getattribute__(self, attr):
        if attr == 'x':
            return 'haha'
        return object.__getattribute__(self, attr)


def main():
    print(Apple().count)
    print(Apple.count)

    b = Bone()
    print(b.count, b.x)
    print(Bone.count)

main()
