#-*- coding: utf8 -*-
from __future__ import print_function


class Base62(object):

    _base = 62
    _chars = '0123456789abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXY'
    _c2n = {}
    _n2c = {}

    for i in range(0, len(_chars)):
        _c2n[_chars[i]] = i
        _n2c[i] = _chars[i]
    del i

    @classmethod
    def encode(kls, num):
        ret = ''
        while True:
            div, mod = divmod(num, kls._base)
            ret = kls._n2c[mod] + ret
            if not div:
                break
            num = div
        return ret

    @classmethod
    def decode(kls, encoded):
        ret = 0
        try:
            for c in encoded:
                ret = ret * kls._base + kls._c2n[c]
        except KeyError:
            raise KeyError('invalid base62 string: %s' % encoded)
        return ret


base62_encode = Base62.encode
base62_decode = Base62.decode


def test():
    n = 129
    c = base62_encode(n)
    assert c == '25', 'encoding error'
    i = base62_decode(c)
    assert i == n, 'decoding error'
