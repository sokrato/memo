from __future__ import print_function


def permute(ls, level=0, cache=None):
    if len(ls) == level:
        print(' '.join(map(lambda i: cache[i], range(level))))
        return
    cache = cache or {}
    for i in ls[level]:
        cache[level] = i
        permute(ls, level+1, cache)

if __name__ == '__main__':
    import sys
    permute(sys.argv[1:])
