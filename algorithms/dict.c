#include <stdio.h>
#include <stdlib.h>

typedef long (*hashFunc)(char*);

typedef struct dictEntry {
    char* key;
    void* data;
    long hash;
} dictEntry;

typedef struct Dict {
    unsigned int mask;
    unsigned int used;
    hashFunc hash;
    dictEntry slots[];
} Dict;

long simple_hash(char* s)
{
    long h = 1;
    do {
        h += (*s << 5) + *s;
    } while( *s++ );
    return h;
}

Dict* dict_new(unsigned int size)
{
    Dict* d = calloc(sizeof(Dict) + size*sizeof(dictEntry), 1);
    return d;
}
int dict_set(Dict* d, char* key, void* data)
{
    return 0;
}


int main(int argc, char* argv[])
{
    while (--argc > 0) {
        ++argv;
        printf("hash(%s) = %ld\n", *argv, simple_hash(*argv));
    }

    return 0;
}
