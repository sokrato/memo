#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    int val;
    struct Node * next;
} Node;

void print_list(Node* head)
{
    for ( ; head; head = head->next) {
        printf("%3d", head->val);
    }
    printf("\n");
}

Node* reverse(Node* head)
{
    Node *p1, *p2;
    if (!head || !head->next) return head;
    p1 = head->next;
    head->next = NULL;
    while (p1) {
        p2 = p1->next;
        p1->next = head;
        head = p1;
        p1 = p2;
    }
    return head;
}

int main(int argc, char* argv[])
{
    int val, i;
    Node* head, *p;
    head = p = NULL;
    for (i=1; i<argc; ++i) {
        val = strtol(argv[i], NULL, 10);
        Node *n = calloc(1, sizeof(Node));
        n->val = val;
        n->next = NULL;
        if (!p) {
            head = p = n;
        } else {
            p->next = n;
            p = n;
        }
    }
    print_list(head);
    head = reverse(head);
    print_list(head);
    return 0;
}
