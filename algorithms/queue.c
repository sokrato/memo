#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int Queue;

#define CAP  0
#define HEAD 1
#define TAIL 2

Queue* q_new(int cap) {
    assert(cap > 0);
    Queue* q = malloc(sizeof(Queue) * (cap + 3));
    assert(q != NULL);
    q[CAP] = cap;
    q[HEAD] = q[TAIL] = -1;
    return q+3;
}

void q_free(Queue *q) {
    free(q-3);
}

void q_push(Queue *q, int v) {
    int* m = q - 3;
    if (m[HEAD] == -1) {
        q[0] = v;
        m[HEAD] = m[TAIL] = 0;
        return;
    }
    if (m[TAIL] == m[CAP] - 1) {
        m[TAIL] = 0;
    } else {
        ++m[TAIL];
    }
    assert(m[TAIL] != m[HEAD]); // overflow
    q[m[TAIL]] = v;
}

int q_pop(Queue *q) {
    int* m = q - 3;
    int v=0;
    assert(m[HEAD] >= 0); // underflow
    v = q[m[HEAD]];
    if (m[HEAD] == m[TAIL]) {
        m[HEAD] = m[TAIL] = -1;
    } else {
        if (m[HEAD] == m[CAP] - 1) {
            m[HEAD] = 0;
        } else {
            ++m[HEAD];
        }
    }
    return v;
}

void q_print(Queue *q) {
    printf("Cap=%d, Head=%d, Tail=%d\n", *(q-3), *(q-2), *(q-1));
}

int main(int argc, char* argv[]) {
    Queue *q = q_new(5);
    int i;
    for (i=1; i < 6; ++i) {
        q_push(q, i);
    }
    q_pop(q);
    q_pop(q);
    q_push(q, 7);
    q_push(q, 8);
    for (i=0; i < 5; ++i) {
        printf("%d, ", q_pop(q));
    }
    printf("\n");
    q_free(q);
    return 0;
}

