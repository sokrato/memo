#include <stdio.h>

// top-down, recursive
int fibo(int n)
{
    if (n<2) return n;
    if (n==2) return 1;
    return fibo(n-2)*2 + fibo(n-3);
}


int main(int argc, char* argv[])
{
    int i;
    for (i=0; i<20; ++i)
        printf("%d, ", fibo(i));
    printf("\n");
    return 0;
}
