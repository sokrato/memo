#include <stdio.h>
#include <stdlib.h>

void diamond(int n)
{
    int level, d=1, s=(n+1)/2, c = 1;
    int i;
    for (level = 1; level<=2*n-1; ++level, d += c, s -= c) {
        for (i=0; i<s; ++i)
            printf(" ");
        for (i=0; i<d; ++i)
            printf("*");
        if (d == n)
            c = -1;
        printf("\n");
    }
}

int main(int argc, char* argv[])
{
    long n = strtol(argv[1], NULL, 10);
    diamond(n);
    return 0;
}

