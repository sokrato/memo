#ifndef DICT_H
#define DICT_H

typedef long (*hashFunc)(char*);
typedef struct dictEntry {
    char* key;
    void* data;
    long hash;
} dictEntry;

typedef struct Dict {
    unsigned int mask;
    //
} Dict;
#endif
