#include <stdio.h>

void fn(int mat[][4], int len)
{
    int x=len-1, y=0, tx;
    for (x=len-1; x>-1; --x) {
        for (tx=x, y=0; tx<len && y<len; ++y, ++tx)
            printf("%3d", mat[y][tx]);
        printf("\n");
    }
    for (y=1; y<len; ++y) {
        for (tx=y, x=0; tx<len && x < len; ++tx, ++x)
            printf("%3d", mat[tx][x]);
        printf("\n");
    }
}

int main(int argc, char* argv[])
{
    int mat[4][4] = {
        { 1, 2, 3, 4 },
        { 5, 6, 7, 8 },
        { 9,10,11,12 },
        {13,14,15,16 }
    };

    fn(mat, 4);
    return 0;
}

