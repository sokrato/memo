#include <stdio.h>

#define HEAP_PARENT(i) ((i-1)/2)
#define HEAP_LEFT(i) (2*i + 1)
#define HEAP_RIGHT(i) (2*i + 2)

void pr(int* arr, int size);
void insertionsort(int* arr, int len);
void heapsort(int arr[], int size);
void mergesort(int arr[], int len);

typedef void (*SortFunc)(int[], int len);

void heapsort2(int *arr, int size) {
    int i, j, p, tmp;
    for (; size > 1; --size, ++arr) {
        for (i=1; i < size; ++i) {
            for (j=i; j > 0; ) {
                p = (j-1) / 2;
                if (arr[p] > arr[j]) {
                    tmp = arr[p];
                    arr[p] = arr[j];
                    arr[j] = tmp;
                }
                j = p;
            }
        }
    }
}

void max_heapify(int *arr, int i, int size) {
    int largest, tmp, left, right;
    for (left=HEAP_LEFT(i); left < size; left=HEAP_LEFT(i)) {
        if (arr[i] < arr[left]) {
            largest = left;
        } else {
            largest = i;
        }

        right = HEAP_RIGHT(i);
        if (right < size && arr[largest] < arr[right]) {
            largest = right;
        }
        if (largest != i) {
            tmp = arr[i];
            arr[i] = arr[largest];
            arr[largest] = tmp;
            i = largest;
        } else {
            break;
        }
    }
}

void build_max_heap(int *arr, int size) {
    int i;
    for (i=(size-1)/2; i>=0; --i) {
        max_heapify(arr, i, size);
    }
}

// bottom up approach
void heapsort3(int *arr, int size) {
    int tmp, i;
    for( ; size > 1; --size) {
        // build_max_heap(arr, size);
        for (i=(size-1)/2; i>=0; --i) {
            max_heapify(arr, i, size);
        }
        tmp = arr[size-1];
        arr[size-1] = arr[0];
        arr[0] = tmp;
    }
}

int main(int argc, char* argv[])
{
    int nums[] = {9, 8, 7, 6, 5, 4, 13, 2, 1, 0, 99, 100, 25, 38, 44, 29};
    build_max_heap(nums, 16);
    pr(nums, 16);

    SortFunc sort=&heapsort3; //&mergesort; //insertionsort;
    sort(nums, 16);
    pr(nums, 16);
    return 0;
}

void pr(int* arr, int size)
{
    int i;
    for (i=0; i<size; ++i)
        printf("%d, ", arr[i]);
    printf("\n");
}

void insertionsort(int* arr, int len)
{
    int i, j;
    int val;
    for (j=1; j<len; ++j) {
        val = arr[j];
        i = j;
        while (--i>-1 && arr[i]>val) {
            arr[i+1] = arr[i];
        }
        arr[i+1] = val;
    }
}

// heapsort begining
void heapify(int arr[], int size)
{
    int i, p, pp;
    for (i=1; i<size; ++i) {
        p = i;
        do {
            pp = (p-1)/2;
            if (arr[pp] > arr[p]) {
                int tmp = arr[p];
                arr[p] = arr[pp];
                arr[pp] = tmp;
                p = pp;
            } else {
                break;
            }
        } while (p>0);
    }
}

void heapsort(int arr[], int size)
{
    int i;
    for (i=0; i<size-2; ++i)
        heapify(arr+i, size-i);
}

// mergesort
void mergesort(int arr[], int len)
{
    if (len <= 1) return;
    int mid = len / 2;
    mergesort(arr, mid);
    mergesort(arr + mid, len - mid);
    // merge
    int i, j, tmp;
    for (i=mid; i<len; ++i) {
        for (j=i; j>0; --j) {
            if (arr[j] >= arr[j-1]) break;
            tmp = arr[j];
            arr[j] = arr[j-1];
            arr[j-1] = tmp;
        }
    }
}
