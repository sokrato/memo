package main

import (
    "fmt"
    "os"
    "io"
    "log"
    "strings"
    "io/ioutil"
    _ "flag"
    "encoding/json"
)

type JSONFormatter struct {
    eol string
    ostream io.Writer
}

func assureNoError(e error) {
    if e != nil {
        fmt.Println(e)
        os.Exit(2)
    }
}

func (jf *JSONFormatter) print(args ...interface{}) {
    fmt.Fprint(jf.ostream, args...)
}

func (jf *JSONFormatter) println(args ...interface{}) {
    jf.print(args...)
    jf.print("\n")
}

func (jf *JSONFormatter) printf(tpl string, args ...interface{}) {
    fmt.Fprintf(jf.ostream, tpl, args...)
}

func (jf *JSONFormatter) PadIndentChar(indent int) {
    for i :=0; i<indent; i++ {
        jf.print("    ")
    }
}

func (jf *JSONFormatter) FormatString(txt string) {
    jf.printf(`"%v"`, strings.Replace(txt, `"`, `\"`, -1))
}

func (jf *JSONFormatter) FormatNumber(n float64) {
    jf.print(n)
}

func (jf *JSONFormatter) FormatBool(b bool) {
    if b {
        jf.print("true")
    } else {
        jf.print("false")
    }
}

func (jf *JSONFormatter) FormatNull() {
    jf.print("null")
}

func (jf *JSONFormatter) FormatArray(arr []interface{}, indent int) {
    jf.print("[")
    length := len(arr) - 1
    newline := false
    for i, v := range arr {
        switch w := v.(type) {
        case map[string]interface{}:
            jf.println()
            jf.FormatObject(w, indent+1)
            newline = true
        case []interface{}:
            jf.println()
            jf.PadIndentChar(indent+1)
            jf.FormatArray(w, indent+1)
            newline = true
        default:
            if i > 0 {
                jf.print(" ")
            }
            jf.FormatData(v, indent)
        }
        if i < length {
            jf.print(",")
        }
    }
    if newline {
        jf.println()
        jf.PadIndentChar(indent)
    }
    jf.print("]")
}

func (jf *JSONFormatter) FormatObject(obj map[string]interface{}, indent int) {
    jf.PadIndentChar(indent)
    jf.println("{")
    length := len(obj) - 1
    n := 0
    for k, v := range obj {
        jf.PadIndentChar(indent+1)
        jf.printf("%v: ", k)
        jf.FormatData(v, indent+1)
        if n < length {
            jf.print(",")
        }
        jf.println()
        n++
    }
    jf.PadIndentChar(indent)
    jf.print("}")
}

func (jf *JSONFormatter) FormatData(data interface{}, indent int) {
    switch w := data.(type) {
    case map[string]interface{}:
        jf.FormatObject(w, indent)
    case []interface{}:
        jf.FormatArray(w, indent)
    case float64:
        jf.FormatNumber(w)
    case string:
        jf.FormatString(w)
    case bool:
        jf.FormatBool(w)
    case nil:
        jf.FormatNull()
    default:
        log.Fatalf("unexpected data: %v", w)
    }
}

func FormatJSON(data interface{}, wr io.Writer) {
    jsfmt := JSONFormatter{"\n", wr}
    jsfmt.FormatData(data, 0)
}

func main() {
    bs, err := ioutil.ReadAll(os.Stdin)
    assureNoError(err)

    var data interface{}
    err = json.Unmarshal(bs, &data)
    assureNoError(err)

    FormatJSON(data, os.Stdout)
    fmt.Println()
}
