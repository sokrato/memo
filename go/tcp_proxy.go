package main

import (
	"flag"
	"io"
	"log"
	"net"
	"strings"
	"sync"
)

var bind, forward string

type Dialer struct {
	net_type, addr string
}

func (d *Dialer) Dial() (net.Conn, error) {
	return net.Dial(d.net_type, d.addr)
}

type Proxy struct {
	bindTo, forwardTo string
	dialer            *Dialer
}

func (p *Proxy) Serve() {
	net_type := "unix"
	if strings.Contains(p.bindTo, ":") {
		net_type = "tcp"
	}
	ln, err := net.Listen(net_type, bind)
	if err != nil {
		log.Printf("failed to start: %v", err)
		return
	}

	log.Printf("Proxy started at %v, forwarding all incoming connections to %v\n", bind, forward)

	var cnt uint32 = 0
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println("accept error:", err)
		} else {
			cnt++
			go p.handleClient(cnt, conn)
		}
	}
}

func (p *Proxy) handleClient(id uint32, conn net.Conn) {
	log.Printf("Conn#%v started.\n", id)
	backend, err := p.dialer.Dial()
	if err != nil {
		log.Println("Backend unreachable:", err)
		return
	}
	var wg sync.WaitGroup
	wg.Add(2)
	go p.pipe(id, "==>", conn, backend, &wg)
	go p.pipe(id, "<==", backend, conn, &wg)
}

func (p *Proxy) pipe(id uint32, dir string, from, to net.Conn, wg *sync.WaitGroup) {
	n, err := io.Copy(from, to)
	log.Printf("%v %v %v\n", dir, n, err)
	wg.Done()
	wg.Wait()
	from.Close()
	/*
		buf := make([]byte, 1024)
		for {
			nread, e := from.Read(buf)
			for i := 0; i < nread; {
				nwrite, e2 := to.Write(buf[i:nread])
				logfile.Write(buf[i:nread])
				if e2 != nil {
					e = e2
					break
				}
				i += nwrite
			}
			if e != nil {
				// log.Printf("Conn#%v disconnected\n", id)
				break
			}
		}
	*/
}

func init() {
	flag.StringVar(&bind, "bind", ":9999", "addr to bind to")
	flag.StringVar(&forward, "forward", "", "addr to forward to")
	flag.Parse()
}

func NewProxy(bindTo, forwardTo string) *Proxy {
	var dialer *Dialer
	if strings.Contains(forward, ":") {
		dialer = &Dialer{"tcp", forward}
	} else {
		dialer = &Dialer{"unix", forward}
	}
	return &Proxy{bindTo, forwardTo, dialer}
}

func main() {
	proxy := NewProxy(bind, forward)
	proxy.Serve()
}
