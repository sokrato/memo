package basen

import (
	"crypto/rand"
	"math/big"
)

func shuffle(bs []byte) {
	max := big.NewInt(int64(len(bs)))
	for i, b := range bs {
		r, _ := rand.Int(rand.Reader, max)
		ri := int(r.Uint64())
		if ri != i {
			bs[i] = bs[ri]
			bs[ri] = b
		}
	}
}
