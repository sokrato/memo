package basen

import (
	"bytes"
	"testing"
)

func TestBadAlphabet(t *testing.T) {
	alpha := "aabbc"
	_, err := NewCoderFromString(alpha)
	if err == nil {
		t.Fatalf("%v should not be valid alphabet", alpha)
	}
}

func TestCoding(t *testing.T) {
	alpha := "abcdefghij"
	c, err := NewCoderFromString(alpha)
	if err != nil {
		t.Fatalf("cannot create Corder from alphabet: %v", alpha)
	}

	bs := c.Encode(12)
	res := []byte{'b', 'c'}
	if !bytes.Equal(bs, res) {
		t.Fatal("bad encoding algo")
	}

	n, err := c.Decode(bs)
	if err != nil || n != 12 {
		t.Fatal("bad decoding algo", n, err)
	}
}

func TestReverseSlice(t *testing.T) {
	a := []byte{'a', 'b'}
	b := []byte{'b', 'a'}
	reverseSlice(a)
	if !bytes.Equal(a, b) {
		t.Fatal("bad reserveSlice")
	}
}
