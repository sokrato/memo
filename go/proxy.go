package main

import (
    "flag"
    "bytes"
    "fmt"
    "io"
    "log"
    "time"
    "net"
    "net/http"
    _ "net/http/httputil"
    "net/url"
    "os"
    "strings"
)

var bind, forward string

type ProxyHandler struct{
    ToAddr string
    Output io.Writer
    tp http.RoundTripper
    idChan chan uint32
}

type Body struct {
    bytes.Buffer
}

func init() {
    flag.StringVar(&bind, "bind", ":9999", "addr to bind to")
    flag.StringVar(&forward, "forward", "", "addr to bind to")
    flag.Parse()

    fmt.Printf("forward=%v\n", forward)
    if forward != "" {
        host, port, err := net.SplitHostPort(forward)
        if err != nil {
            log.Fatal(err)
        }
        if host == "" {
            forward = net.JoinHostPort("localhost", port)
        }
    }
}

func main() {
    handler := NewProxyHandler(forward, os.Stdout)
    server := &http.Server {
        Addr: bind,
        Handler: handler,
        ReadTimeout: 10 * time.Second,
        WriteTimeout: 10 * time.Second,
        MaxHeaderBytes: 1 << 20,
    }

    go func() {
        fmt.Printf("Proxy started at %v, forwarding all incoming requests to %v\n", bind, forward)
    }()

    log.Fatal(server.ListenAndServe())
}

func (b *Body) Close() error {
    return nil
}

func NewProxyHandler(forwardto string, output io.Writer) *ProxyHandler {
    handler := ProxyHandler{
        ToAddr: forward,
        Output: output,
        tp: &http.Transport{
            Proxy: http.ProxyFromEnvironment,
            Dial: (&net.Dialer{
                Timeout:  30 * time.Second,
                KeepAlive: 30 * time.Second,
            }).Dial,
        },
        idChan: make(chan uint32, 1),
    }
    go func() {
        var id uint32
        for {
            id ++
            handler.idChan <- id
        }
    }()

    return &handler
}

func (p *ProxyHandler) NextId() uint32 {
    return <- p.idChan
}

func (p *ProxyHandler) logRequest(id uint32, r *http.Request) {
    fmt.Fprintf(p.Output, "=== Request #%v ===\n", id)
    fmt.Fprintf(p.Output, "%v %v %v\n", r.RemoteAddr, r.Method, r.URL)

    r.Header.Write(p.Output)
    fmt.Fprintln(p.Output, "Host: ", r.Host)
    fmt.Fprintln(p.Output)

    if ! strings.HasPrefix(r.Header.Get("Content-Type"), "multipart/form-data;") {
        body := &Body{}
        io.Copy(body, r.Body)
        bs := body.Bytes()

        p.Output.Write(bs)
        fmt.Fprintln(p.Output)
        r.Body = body
    }
}

func (p *ProxyHandler) logResponse(id uint32, r *http.Response) {
    fmt.Fprintf(p.Output, "=== Response #%v ===\n", id)
    fmt.Fprintf(p.Output, "%v\n", r.Status)
    r.Header.Write(p.Output)
    fmt.Fprintln(p.Output)

    if r.Header.Get("Content-Type") == "application/json" {
        buf := &Body{}
        io.Copy(buf, r.Body)
        bs := buf.Bytes()

        p.Output.Write(bs)
        fmt.Fprintln(p.Output)
        r.Body = buf
    }
}

func (p *ProxyHandler) ForwardRequest(r *http.Request) (*http.Response, error) {
    url := &url.URL{
        Scheme: "http", //r.URL.Scheme,
        Opaque: r.URL.Opaque,
        User: r.URL.User,
        Path: r.URL.Path,
        RawQuery: r.URL.RawQuery,
        Fragment: r.URL.Fragment,
    }
    if p.ToAddr == "" {
        url.Host = r.Host
    } else {
        url.Host = p.ToAddr
    }

    var buf bytes.Buffer
    io.Copy(&buf, r.Body)
    req, err := http.NewRequest(r.Method, url.String(), &buf)

    for k, vs := range r.Header {
        for _, v := range vs {
            req.Header.Add(k, v)
        }
    }
    req.Header.Set("Host", r.Host)
    req.Host = r.Host
    if err != nil {
        return nil, err
    }

    resp, err := p.tp.RoundTrip(req)
    if err != nil {
        return nil, err
    }
    return resp, nil
}

func (p *ProxyHandler) ForwardResponse(w http.ResponseWriter, resp *http.Response) (e error) {
    headers := w.Header()
    for k, vs := range resp.Header {
        for _, v := range vs {
            headers.Add(k, v)
        }
    }
    w.WriteHeader(resp.StatusCode)
    _, e = io.Copy(w, resp.Body)
    return
}

func (p *ProxyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    id := p.NextId()
    p.logRequest(id, r)
    defer r.Body.Close()

    resp, err := p.ForwardRequest(r)
    if err != nil {
        fmt.Println(err)
        w.WriteHeader(http.StatusInternalServerError)
        return
    }
    defer resp.Body.Close()
    p.logResponse(id, resp)

    err = p.ForwardResponse(w, resp)
    if err != nil {
        fmt.Println("ForwardResponse:", err)
    }
}
