package main

import (
    "flag"
    "fmt"
    "net/http"
    "os"
)

var addr, dir, prefix string

func init() {
    flag.StringVar(&addr, "bind", ":8888", "where to bind")
    flag.StringVar(&dir, "dir", ".", "which dir to serve")
    flag.StringVar(&prefix, "prefix", "", "strip some prefix?")
    flag.Parse()
}

func err(e error) {
    if e != nil {
        fmt.Println(e)
        os.Exit(1)
    }
}

func main() {
    handler := http.FileServer(http.Dir(dir))
    if prefix != "" {
        handler = http.StripPrefix(prefix, handler)
    }
    fmt.Printf("Starting at: %v, dir=%v, prefix=%v\n", addr, dir, prefix)
    err(http.ListenAndServe(addr, handler))
}
