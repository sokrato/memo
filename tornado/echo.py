from __future__ import print_function

from tornado.ioloop import IOLoop
from collections import deque
import socket
import errno
import os
import sys
import logging
import functools


def merge_head(deq, size):
    pass


class EchoServer(object):
    def __init__(self, port=None, host='0.0.0.0', ioloop=None):
        self.socks = EchoServer.bind_sockets(port, host)
        self.ioloop = ioloop or IOLoop.instance()
        for fd, sock in self.socks.items():
            self.ioloop.add_handler(fd, self.connection_ready, IOLoop.READ)
        self.logger = EchoServer.create_logger('EchoServer')
        self.READ_BUFF_LEN = 16
        self._msg_buff = {} # fd: mesg q
        self._conns = {} # fd: socket

    @classmethod
    def bind_sockets(self, port, host):
        socks = {}
        ais = socket.getaddrinfo(host, port, socket.AF_INET, socket.SOCK_STREAM, 0, socket.AI_PASSIVE)
        for ai in ais:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind(ai[-1])
            sock.setblocking(False)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.listen(socket.SOMAXCONN)
            socks[sock.fileno()] = sock
        return socks

    @classmethod
    def create_logger(self, name):
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        sh = logging.StreamHandler(sys.stderr)
        sh.setLevel(logging.DEBUG)
        fmt = logging.Formatter('%(levelname)s %(name)s %(asctime)s : %(message)s')
        sh.setFormatter(fmt)
        logger.addHandler(sh)
        logger.propagate = False
        return logger

    def connection_ready(self, fd, event):
        while True:
            try:
                client, addr = self.socks[fd].accept()
            except Exception as exc:
                if exc.args[0] in (errno.EWOULDBLOCK, errno.EAGAIN):
                    return
                raise
            client.setblocking(False)
            self.handle_connection(client, addr)

    def handle_connection(self, client, addr):
        fd = client.fileno()
        self.logger.debug("Connection from {}, fd={}".format(addr, fd))
        self._conns[fd] = client
        self._msg_buff[fd] = deque()
        self.ioloop.add_handler(fd, self.handle_io, IOLoop.READ)
        # self.handle_io(fd, IOLoop.READ)

    def handle_io(self, fd, events):
        self.logger.debug("fd %s event %s" % (fd, events))
        if events & IOLoop.READ:
            self._handle_read(fd)
        if events & IOLoop.WRITE:
            self._handle_write(fd)
        if events & IOLoop.ERROR:
            self._handle_error(fd)

    def _handle_read(self, fd):
        buff = self._msg_buff.get(fd)
        while True:
            try:
                msg = os.read(fd, self.READ_BUFF_LEN)
                if not msg or msg.startswith('QUIT'): # EOF or QUIT
                    self.logger.debug("msg = *{}*".format(msg))
                    self.close_client(fd)
                    return
            except Exception as e:
                if e.args[0] in (errno.EWOULDBLOCK, errno.EAGAIN):
                    break
                self.logger.error(e)
                raise
            buff.append(msg)
        if buff:
            self.ioloop.update_handler(fd, IOLoop.READ|IOLoop.WRITE)

    def _handle_write(self, fd):
        msgq = self._msg_buff.get(fd, None)
        while msgq:
            try:
                nwriten = os.write(fd, msgq.popleft())
                # TODO
            except Exception as e:
                if e.args[0] in (errno.EWOULDBLOCK, errno.EAGAIN):
                    return
                elif e.args[0] in (errno.ECONNRESET, errno.ECONNABORTED,
                                   errno.EPIPE):
                    self.logger.debug("write error")
                    self.close_client(fd)
                raise
            except IndexError:
                self.logger.debug('no more message')
                self.close_client(fd)
        if not msgq:
            self.ioloop.update_handler(fd, IOLoop.READ)

    def _handle_error(self, fd):
        self.logger.warn("error with ", fd)
        self.close_client(fd)

    def close_client(self, fd):
        conn = self._conns.pop(fd, None)
        if conn:
            conn.close()
            self.ioloop.remove_handler(fd)
            self._msg_buff.pop(fd, None)
            self.logger.debug("fd {} disconnected".format(fd))

    def start(self):
        try:
            addrs = '\n'.join([str(s.getsockname()) for fd, s in self.socks.items()])
            self.logger.info("EchoServer starting at {}".format(addrs))
            self.ioloop.start()
        except:
            self.stop()
            raise

    def stop(self):
        for fd, sock in self.socks.items():
            sock.close()
        self.ioloop.stop()


if __name__ == '__main__':
    serv = EchoServer()
    serv.start()
