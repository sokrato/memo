# -*- encoding: utf-8 -*-
from __future__ import print_function, absolute_import

import sys
import os
import errno
import signal
import select
import socket
import threading, thread
import logging


class IOLoop(object):
    # Constants from the epoll module
    _EPOLLIN  = 0x001
    _EPOLLPRI = 0x002
    _EPOLLOUT = 0x004
    _EPOLLERR = 0x008
    _EPOLLHUP = 0x010
    _EPOLLRDHUP = 0x2000
    _EPOLLONESHOT = (1 << 30)
    _EPOLLET = (1 << 31)

    # Our events map exactly to the epoll events
    NONE = 0
    READ = _EPOLLIN
    WRITE = _EPOLLOUT
    ERROR = _EPOLLERR | _EPOLLHUP

    def __init__(self):
        pass

    def add_handler(self, fd, evt, handler):
        raise NotImplementedError

    def update_handler(self, fd, evt):
        raise NotImplementedError

    def remove_handler(self, fd):
        raise NotImplementedError

    def start(self):
        raise NotImplementedError

    def stop(self):
        raise NotImplementedError


class PollIOLoop(IOLoop):
    _instance = None
    _init_lock = threading.Lock()

    def __init__(self):
        self._imp = self.make_imp()
        self._handlers = {}
        self._events = {}
        self._stopped = False
        self._running = False
        self.logger = logging.getLogger('ioloop')
        if not self.logger.handlers:
            logging.basicConfig()

    def make_imp(self):
        pass # return select.Poll()

    def start(self):
        poll_timeout = 0
        while True:
            try:
                event_pairs = self._imp.poll(poll_timeout)
            except Exception as e:
                if (getattr(e, 'errno', None) == errno.EINTR or
                     isinstance(getattr(e, 'args', None), tuple) and
                     len(e.args) == 2 and e.args[0] == errno.EINTR):
                    continue
                else:
                    raise
            self._events.update(event_pairs)
            while self._events:
                fd, evt = self._events.popitem()
                try:
                    self._handlers[fd](fd, evt)
                    # no need to remove fd if error occured?
                except (OSError, IOError) as e:
                    if e.args[0] == errno.EPIPE:
                        pass
                    else:
                        self.logger.error('Exception in I/O handler for fd %s',
                                          fd, exc_info=True)
                except Exception:
                    self.logger.error('Exception in I/O handler for fd %s',
                                      fd, exc_info=True)

            if self._stopped:
                break
        pass # finalize work

    def add_handler(self, fd, event, handler):
        self._handlers[fd] = handler
        self._imp.register(fd, event | IOLoop.ERROR)

    def update_handler(self, fd, event):
        self._imp.modify(fd, event | IOLoop.ERROR)

    def remove_handler(self, fd):
        self._handlers.pop(fd, None)
        self._events.pop(fd, None)
        try:
            self._imp.unregister(fd)
        except Exception:
            self.logger.error("error removing handler", exc_info=True)

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = EpollIOLoop()
        return cls._instance


class EpollIOLoop(PollIOLoop):
    def make_imp(self):
        return select.epoll()

class TcpServer(object):
    RESPONSE = '''HTTP/1.1 200 OK\r\nServer: Tengine\r\nDate: Tue, 10 Dec 2013 08:14:13 GMT\r\nContent-Type: text/html; charset=utf-8\r\nConnection: close\r\nVary: Cookie\r\n\r\n<!doctype html><html lang="en"><head><title>hello</title></head><body><h1>Hello</h1></body></html>'''

    def __init__(self, ioloop=None, port=8080, host=None):
        self.ioloop = ioloop
        self.port = port
        self.host = host or ''
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setblocking(0)
        self._conn = {}

    def start(self):
        self.socket.bind((self.host, self.port))
        self.socket.listen(socket.SOMAXCONN)
        self.ioloop.add_handler(self.socket.fileno(), IOLoop.READ, self._handle_connection)

    def stop(self):
        self.socket.close()

    def _handle_connection(self, fd, evt):
        while True:
            try:
                conn, addr = self.socket.accept()
            except socket.error as e:
                if e.args[0] in (errno.EAGAIN, errno.EWOULDBLOCK):
                    break
                else:
                    raise
            print("Connection from (%s, %s)" % addr)
            cfd = conn.fileno()
            conn.setblocking(0)
            self._conn[cfd] = conn
            self.ioloop.add_handler(cfd, IOLoop.READ, self.handle_stream)

    def handle_stream(self, fd, evt):
        conn = self._conn[fd]
        if evt & IOLoop.READ:
            print("Message from client:")
            print(conn.recv(1024))
            self.ioloop.update_handler(fd, IOLoop.WRITE)
        else:
            if evt & IOLoop.WRITE:
                conn.send(self.RESPONSE)
            self._conn.pop(fd, None)
            conn.close()
            self.ioloop.remove_handler(fd)

if '__main__' == __name__:
    ioloop = EpollIOLoop()
    server = TcpServer(ioloop)
    try:
        server.start()
        ioloop.start()
    except Exception as e:
        print("error occured", e)
    finally:
        server.stop()
