from __future__ import print_function, with_statement, division
import select
import socket
import errno
import sys


def prep(port=8080):
    ep = select.epoll(1)

    ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ss.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    ss.setblocking(False)
    ss.bind(('', 8080))
    ss.listen(socket.SOMAXCONN)

    ep.register(ss.fileno(), select.EPOLLIN)
    try:
        print("try")
        return ep, ss
    finally:
        print("finally")


def main():
    try:
        ep, ss = prep()
        socks = {}
        while True:
            event_pairs = ep.poll()
            while event_pair:
                fd, event = event.pop()
                if ss.fileno() == fd:
                    try:
                        cs, addr = ss.accept()
                    except socket.error as e:
                        if e.args[0] in (errno.EAGAIN,
                                         errno.EWOULDBLOCK,
                                         errno.ECONNABORTED):
                        print("accept error", e)
                        continue
                    print("connection from ", addr)
                    cs.setblocking(False)
                    socks[cs.fileno()] = cs
                    ep.register(cs.fileno(), select.EPOLLIN|select.EPOLLERR|select.EPOLLHUP)
                conn = socks[fd]
                if event & select.EPOLLIN:
                    try:
                        print(conn.recv(1024))
                    except socket.error as e:
                        if e.args[0] in (errno.EAGAIN,
                                         errno.EWOULDBLOCK):
                            continue
                        raise
                elif event & select.EPOLLOUT:
                    try:
                else: # EPOLLHUP or EPOLLERR
                    print("WTF!? ", fd, event)
                    socks.pop(fd, None)
    finally:
        ss.close()
        ep.close()


if '__main__' == __name__:
    main()
