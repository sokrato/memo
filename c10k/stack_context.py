from __future__ import print_function

import threading
import functools
import collections


class Task(object):
    pass


class TaskRunner(threading.Thread):
    def __init__(self, ident, *args, **kwargs):
        super(TaskRunner, self).__init__(*args, **kwargs)
        self.ident = ident
        self._stop = False

        def run(self):
            while not self._stop:
            try:
            task = self.fetch_task

        def fetch_task(self):
            pass


class TaskScheduler(object):
    cond = threading.Condition()
    tasks = collections.deque()

    def __init__(self, pool_size=4):

def _fn(id=None):
    print("Runner %d started" % id)
    while True:
        cond.acquire()
        while len(tasks) == 0:
            cond.wait()
        task = tasks.popleft()
        try:
            print("%s running" % id)
            task()
        except Exception as e:
            print("error in %s: " % id, e)
        cond.release()


_cnt = 0
def addTaskRunner(num=1):
    global _cnt
    while num > 0:
        _cnt += 1
        th = threading.Thread(target=_fn, args=(_cnt, ))
        th.daemon = True
        th.start()
        num -= 1


def add_task(task, *args, **kwargs):
    cond.acquire()
    tasks.append(functools.partial(task, *args, **kwargs))
    cond.notify(1)
    cond.release()


def main():
    def t():
        print(100*100)
    addTaskRunner(2)
    add_task(t)


if __name__ == '__main__':
    main()
    scheduler = ...
    result = scheduler.sync(fn)
    scheduler.async(fn, callback)
    // eventloop.start()
